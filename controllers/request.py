#Project Name: ONVOLO
#Company: ONVOLO LTD
#Description: Social Posting Site
#Created By: JP Mendoza
#

import urllib
import urllib2, json

def api():

    _post = request.post_vars
    command = request.args[0]
    #return _post['keywords']
    if _post :

        if command == 'search':
            datas = {'keywords' : _post['keywords'], 'page' : 1, 'type' : 'my_searches',
                                'token' : _token, 'apikey' : _apikey}
        elif command == 'search-autocomplete':
            datas = {'q' : _post['keywords'], 'token' : _token, 'apikey' : _apikey}
        elif command == 'search-explore':
            datas = {'skip' : 0, 'limit' : 20, 'apikey' : _apikey}
        elif command == 'user-login':
            datas = {'email' : _post['email'], 'password' : _post['password'], 'media_login' : 0, 'apikey' : _apikey}
        elif command == 'post':
            datas = {'title' : _post['title'], 'category' : _post['category'], 'description' : _post['description'],
                     'location' : _post['location'], 'price' : _post['price'], 'status' : _post['status'], 'accept_volo_dollars' : _post['accept'],
                     'token' : _token, 'apikey' : _apikey}
            
        _urlrequest = str.replace(command, '-', '/')
        jsn = request_api(_urlrequest, datas)
        return json.dumps(jsn)

def upload():
    reqq = request.vars
    d = {'post_id' : reqq['post_id'], 'post_image' : reqq['post_image']}
    
    return  json.dumps(d)
    
def test():
    a = session.mysess
    
    return a['token']
