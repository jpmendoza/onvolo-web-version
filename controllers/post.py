#Project Name: ONVOLO
#Company: ONVOLO LTD
#Description: Social Posting Site
#Created By: JP Mendoza
#

import urllib
import urllib2, json

def view():
    req = request.args
    ##if req[0]:
    _id = req[0]
    datas = {'post_id' : _id,  'apikey' : _apikey}
        
    jsn = request_api('post/view', datas)
    #return json.dumps(jsn)
    if jsn['success'] == True:
        _post = jsn['post_details']
        response.post_title = _post['title']
        response.post_description = _post['description']
        response.post_status = _post['status']
        response.post_location = _post['location']
        response.post_price = _post['price']
        response.post_category = request_categories(_post['category'])
        response.post_image = str(_post_img_url)+'400x300/'+str(_post['post_images'][0]['image_name'])
        response.post_images = _post['post_images']
        response.post_image_url = str(_post_img_url)
    return dict()

def create():
    return dict()

def categ():
    return request_categories(0, False)
