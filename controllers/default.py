#Project Name: ONVOLO
#Company: ONVOLO LTD
#Description: Social Posting Site
#Created By: JP Mendoza
#

import urllib
import urllib2, json

def index():
    
    datas = datas = {'skip' : 0, 'limit' : 20, 'apikey' : _apikey}
    _request = 'search/explore'
    arr = request_api(_request, datas)


    #image size
    maxHT=0
    maxWT=0
    imgWT=0
    imgHT=0
    HT=0
    WT=0
    FWT=0
    FHT=0
    ratio=0
    column=4

    browser_WT = 1349
    post_container = float(browser_WT) * .9
    post_count = len(arr['results']) - 1 if "results" in arr else 0
    post_margin=10

    maxWT = (post_container - (column * 10)) / column
    maxHT = maxWT

    #return json.dumps(arr)
    c=0
    postss = []
    for i in range(post_count):
        if 'feed' in arr['results'][c]:
            posts = {}
            post = arr['results'][c]['feed']
    
            post_img_url = 'http://54.193.15.236/volo_photo/resize/index/post/'
            profile_img_url = 'http://54.193.15.236/volo_photo/resize/index/profile/60x60/'+post['user_profile_image']
    
            #calculate image size
            imgWT = post['width']
            imgHT = post['height']
    
            if imgWT > imgHT: #landscape
                if imgWT > maxWT:
                    ratio = maxWT / imgWT
                    HT = imgHT * ratio
                elif maxWT > imgWT:
                    ratio = imgWT / maxWT
                    HT = maxHT * ratio
                WT=maxWT
            elif imgHT > imgWT: #portrait
                if imgHT > maxHT:
                    ratio = maxHT / imgHT
                    WT = imgWT * ratio
                elif maxHT > imgHT:
                    ratio = imgxHT / maxHT
                    WT = maxWT * ratio
                HT=maxHT
            elif imgWT == imgHT: #square
                WT=maxWT
                HT=maxHT
                
            FWT = int(WT)
            FHT = int(HT)
    
            dimension = str(FWT)+'x'+str(FHT)+'/'
            img_url = post_img_url+dimension+post['image_url']
            if post['image_url'] != '':
                posts['_id_'] = post['_id_']
                posts['title'] = post['title']
                posts['image'] = img_url
                posts['profile'] = profile_img_url
                posts['price'] = post['price']
                posts['divHT'] = FHT
                posts['divWT'] = FWT
            
                postss.append(posts)
            c+= 1

    response.post_items = postss#len(ar['results']) #ar["results"][0]['feed']
    #items = request_api(_request, datas)
    
    return dict()

def ss():
    return dict()
