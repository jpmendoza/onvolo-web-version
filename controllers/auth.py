#Project Name: ONVOLO
#Company: ONVOLO LTD
#Description: Social Posting Site
#Created By: JP Mendoza
#

import urllib
import urllib2, json

def register():
    _post = request.post_vars
    
    if 'email' and 'password' in _post:
        datas = {'email' : _post['email'], 'password' : _post['password'], 'firstname' : _post['firstname'], 'lastname' : _post['lastname'],
                 'gender' : _post['gender'], 'birthdate' : _post['dob'], 'media_signup' : 0, 'apikey' : _apikey}
        
        jsn = request_api('user/register', datas)
        
        if jsn['success'] == True:
            rtn = jsn
        else:
            rtn = {'success' : jsn['errors']}
    else:
        rtn = {'success' : False}
    return json.dumps(rtn)

def login():
    _post = request.post_vars

    if 'email' and 'password' in _post:
        datas = {'email' : _post['email'], 'password' : _post['password'], 'media_login' : 0, 'apikey' : _apikey}

        jsn = request_api('user/login', datas)

        if jsn['success'] == True:
            session.loggedin = True
            session.user_data = jsn
            session.user_token = jsn['token']
            rtn = {'success' : True}
        else:
            rtn = {'success' : False}
    else:
        rtn = {'success' : False}
    return json.dumps(rtn)

def logout():
    if session.loggedin:
        datas = {'token' : _token, 'apikey' : _apikey}
        jsn = request_api('user/logout', datas)
        
        if jsn['success'] == True:
            session.loggedin = False
            session.user_data = {}
            session.user_token = ''
            rtn = {'success' : True}
            return dict()
        else:
            rtn = {'success' : 'error'}
            return json.dumps(rtn)
    else:
        rtn = {'success' : 'error'}
        return json.dumps(rtn)

def logform():
    return dict()

def regsform():
    return dict()

def test():
    session.mysess = {'token':'test1', 'name' : 'test2'}
    a = session.mysess

    return a
