# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)


#custom set
uri = {}
uri['query'] = 'http://54.193.14.166/volo_api/search/'

_apiurl = 'http://54.193.14.166/volo_api/'
_uploadurl = 'http://54.193.15.236/volo_photo/upload/testupload/123'

#image url
_post_img_url = 'http://54.193.15.236/volo_photo/resize/index/post/'
_profile_img_url = 'http://54.193.15.236/volo_photo/resize/index/profile/60x60/'

#Guest Token
_token = session.user_token if session.loggedin else 0 #'MU5yYkF4TGFkZ0RCb1BJanFTZ0E'

#Apps Api Key
_apikey = 'abcedfgvolo0987654321apikey'

response.site_url = 'http://localhost:4100/volo/' #'http://54.193.29.33/volo/' #'http://localhost:4100/volo/'

def request_api(command, datas):
    data = urllib.urlencode(datas)
    req = urllib2.Request(_apiurl + command, data)
    res = urllib2.urlopen(req)
    json_result = res.read()
    return  json.loads(json_result)

#get categories
def request_categories(cat=0, text=True):
    datas = {'apikey' : _apikey}
    jsn = request_api('post/get_categories', datas)
    #return jsn
    if jsn['success'] == True:
        if cat != 0:
            if text:
                rtn = jsn['categories'][(cat-1)][str(cat)]
            else:
                rtn = jsn['categories'][(cat-1)].keys()[0]
        else:
            rtn = json.dumps(jsn['categories'])
                
    return rtn
    

def post(var=''):
    if var == '':
        return request.post_vars[var]
    else:
        return request.post_vars
