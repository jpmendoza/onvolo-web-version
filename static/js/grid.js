(function (b) {
    var a = {
        init: function (f) {
            var e = function (g) {
                var h = b(g);
                h.off(".killPlace").on("focus.killPlace", function () {
                    a.onFocus.apply(h)
                }).on("blur.killPlace", function () {
                    a.onBlur.apply(h)
                })
            }, c = this.length,
                d = 0;
            if (c > 1) {
                for (d = 0; d < c; d++) {
                    e(this[d])
                }
                return
            }
            if (c == 1) {
                e(this)
            }
        },
        onFocus: function () {
            var c = b(this);
            c.data("placeholder", c.attr("placeholder"));
            c.attr("placeholder", "")
        },
        onBlur: function () {
            var c = b(this);
            c.attr("placeholder", c.data("placeholder"))
        },
        destroy: function () {
            var c = b(this);
            c.off(".killPlace").removeData(".killPlace")
        }
    };
    b.fn.killPlace = function (c) {
        if (a[c]) {
            return a[c].apply(this, Array.prototype.slice.call(arguments, 1))
        } else {
            if (typeof c === "object" || !c) {
                return a.init.apply(this, arguments)
            } else {
                b.error("Method " + c + " does not exist");
                return false
            }
        }
    }
})(jQuery);
typeof JSON != "object" && (JSON = {}),
function () {
    "use strict";

    function f(e) {
        return e < 10 ? "0" + e : e
    }

    function quote(e) {
        return escapable.lastIndex = 0, escapable.test(e) ? '"' + e.replace(escapable, function (e) {
            var t = meta[e];
            return typeof t == "string" ? t : "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
        }) + '"' : '"' + e + '"'
    }

    function str(e, t) {
        var n, r, i, s, o = gap,
            u, a = t[e];
        a && typeof a == "object" && typeof a.toJSON == "function" && (a = a.toJSON(e)), typeof rep == "function" && (a = rep.call(t, e, a));
        switch (typeof a) {
        case "string":
            return quote(a);
        case "number":
            return isFinite(a) ? String(a) : "null";
        case "boolean":
        case "null":
            return String(a);
        case "object":
            if (!a) return "null";
            gap += indent, u = [];
            if (Object.prototype.toString.apply(a) === "[object Array]") {
                s = a.length;
                for (n = 0; n < s; n += 1) u[n] = str(n, a) || "null";
                return i = u.length === 0 ? "[]" : gap ? "[\n" + gap + u.join(",\n" + gap) + "\n" + o + "]" : "[" + u.join(",") + "]", gap = o, i
            }
            if (rep && typeof rep == "object") {
                s = rep.length;
                for (n = 0; n < s; n += 1) typeof rep[n] == "string" && (r = rep[n], i = str(r, a), i && u.push(quote(r) + (gap ? ": " : ":") + i))
            } else
                for (r in a) Object.prototype.hasOwnProperty.call(a, r) && (i = str(r, a), i && u.push(quote(r) + (gap ? ": " : ":") + i));
            return i = u.length === 0 ? "{}" : gap ? "{\n" + gap + u.join(",\n" + gap) + "\n" + o + "}" : "{" + u.join(",") + "}", gap = o, i
        }
    }
    typeof Date.prototype.toJSON != "function" && (Date.prototype.toJSON = function (e) {
        return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null
    }, String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function (e) {
        return this.valueOf()
    });
    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap, indent, meta = {
            "\b": "\\b",
            " ": "\\t",
            "\n": "\\n",
            "\f": "\\f",
            "\r": "\\r",
            '"': '\\"',
            "\\": "\\\\"
        }, rep;
    typeof JSON.stringify != "function" && (JSON.stringify = function (e, t, n) {
        var r;
        gap = "", indent = "";
        if (typeof n == "number")
            for (r = 0; r < n; r += 1) indent += " ";
        else typeof n == "string" && (indent = n);
        rep = t;
        if (!t || typeof t == "function" || typeof t == "object" && typeof t.length == "number") return str("", {
            "": e
        });
        throw new Error("JSON.stringify")
    }), typeof JSON.parse != "function" && (JSON.parse = function (text, reviver) {
        function walk(e, t) {
            var n, r, i = e[t];
            if (i && typeof i == "object")
                for (n in i) Object.prototype.hasOwnProperty.call(i, n) && (r = walk(i, n), r !== undefined ? i[n] = r : delete i[n]);
            return reviver.call(e, t, i)
        }
        var j;
        text = String(text), cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function (e) {
            return "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
        }));
        if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) return j = eval("(" + text + ")"), typeof reviver == "function" ? walk({
            "": j
        }, "") : j;
        throw new SyntaxError("JSON.parse")
    })
}(),
function (e, t) {
    "use strict";
    var n = e.History = e.History || {}, r = e.jQuery;
    if (typeof n.Adapter != "undefined") throw new Error("History.js Adapter has already been loaded...");
    n.Adapter = {
        bind: function (e, t, n) {
            r(e).bind(t, n)
        },
        trigger: function (e, t, n) {
            r(e).trigger(t, n)
        },
        extractEventData: function (e, n, r) {
            var i = n && n.originalEvent && n.originalEvent[e] || r && r[e] || t;
            return i
        },
        onDomLoad: function (e) {
            r(e)
        }
    }, typeof n.init != "undefined" && n.init()
}(window),
function (e, t) {
    "use strict";
    var n = e.document,
        r = e.setTimeout || r,
        i = e.clearTimeout || i,
        s = e.setInterval || s,
        o = e.History = e.History || {};
    if (typeof o.initHtml4 != "undefined") throw new Error("History.js HTML4 Support has already been loaded...");
    o.initHtml4 = function () {
        if (typeof o.initHtml4.initialized != "undefined") return !1;
        o.initHtml4.initialized = !0, o.enabled = !0, o.savedHashes = [], o.isLastHash = function (e) {
            var t = o.getHashByIndex(),
                n;
            return n = e === t, n
        }, o.isHashEqual = function (e, t) {
            return e = encodeURIComponent(e).replace(/%25/g, "%"), t = encodeURIComponent(t).replace(/%25/g, "%"), e === t
        }, o.saveHash = function (e) {
            return o.isLastHash(e) ? !1 : (o.savedHashes.push(e), !0)
        }, o.getHashByIndex = function (e) {
            var t = null;
            return typeof e == "undefined" ? t = o.savedHashes[o.savedHashes.length - 1] : e < 0 ? t = o.savedHashes[o.savedHashes.length + e] : t = o.savedHashes[e], t
        }, o.discardedHashes = {}, o.discardedStates = {}, o.discardState = function (e, t, n) {
            var r = o.getHashByState(e),
                i;
            return i = {
                discardedState: e,
                backState: n,
                forwardState: t
            }, o.discardedStates[r] = i, !0
        }, o.discardHash = function (e, t, n) {
            var r = {
                discardedHash: e,
                backState: n,
                forwardState: t
            };
            return o.discardedHashes[e] = r, !0
        }, o.discardedState = function (e) {
            var t = o.getHashByState(e),
                n;
            return n = o.discardedStates[t] || !1, n
        }, o.discardedHash = function (e) {
            var t = o.discardedHashes[e] || !1;
            return t
        }, o.recycleState = function (e) {
            var t = o.getHashByState(e);
            return o.discardedState(e) && delete o.discardedStates[t], !0
        }, o.emulated.hashChange && (o.hashChangeInit = function () {
            o.checkerFunction = null;
            var t = "",
                r, i, u, a, f = Boolean(o.getHash());
            return o.isInternetExplorer() ? (r = "historyjs-iframe", i = n.createElement("iframe"), i.setAttribute("id", r), i.setAttribute("src", "#"), i.style.display = "none", n.body.appendChild(i), i.contentWindow.document.open(), i.contentWindow.document.close(), u = "", a = !1, o.checkerFunction = function () {
                if (a) return !1;
                a = !0;
                var n = o.getHash(),
                    r = o.getHash(i.contentWindow.document);
                return n !== t ? (t = n, r !== n && (u = r = n, i.contentWindow.document.open(), i.contentWindow.document.close(), i.contentWindow.document.location.hash = o.escapeHash(n)), o.Adapter.trigger(e, "hashchange")) : r !== u && (u = r, f && r === "" ? o.back() : o.setHash(r, !1)), a = !1, !0
            }) : o.checkerFunction = function () {
                var n = o.getHash() || "";
                return n !== t && (t = n, o.Adapter.trigger(e, "hashchange")), !0
            }, o.intervalList.push(s(o.checkerFunction, o.options.hashChangeInterval)), !0
        }, o.Adapter.onDomLoad(o.hashChangeInit)), o.emulated.pushState && (o.onHashChange = function (t) {
            var n = t && t.newURL || o.getLocationHref(),
                r = o.getHashByUrl(n),
                i = null,
                s = null,
                u = null,
                a;
            return o.isLastHash(r) ? (o.busy(!1), !1) : (o.doubleCheckComplete(), o.saveHash(r), r && o.isTraditionalAnchor(r) ? (o.Adapter.trigger(e, "anchorchange"), o.busy(!1), !1) : (i = o.extractState(o.getFullUrl(r || o.getLocationHref()), !0), o.isLastSavedState(i) ? (o.busy(!1), !1) : (s = o.getHashByState(i), a = o.discardedState(i), a ? (o.getHashByIndex(-2) === o.getHashByState(a.forwardState) ? o.back(!1) : o.forward(!1), !1) : (o.pushState(i.data, i.title, encodeURI(i.url), !1), !0))))
        }, o.Adapter.bind(e, "hashchange", o.onHashChange), o.pushState = function (t, n, r, i) {
            r = encodeURI(r).replace(/%25/g, "%");
            if (o.getHashByUrl(r)) throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");
            if (i !== !1 && o.busy()) return o.pushQueue({
                scope: o,
                callback: o.pushState,
                args: arguments,
                queue: i
            }), !1;
            o.busy(!0);
            var s = o.createStateObject(t, n, r),
                u = o.getHashByState(s),
                a = o.getState(!1),
                f = o.getHashByState(a),
                l = o.getHash(),
                c = o.expectedStateId == s.id;
            return o.storeState(s), o.expectedStateId = s.id, o.recycleState(s), o.setTitle(s), u === f ? (o.busy(!1), !1) : (o.saveState(s), c || o.Adapter.trigger(e, "statechange"), !o.isHashEqual(u, l) && !o.isHashEqual(u, o.getShortUrl(o.getLocationHref())) && o.setHash(u, !1), o.busy(!1), !0)
        }, o.replaceState = function (t, n, r, i) {
            r = encodeURI(r).replace(/%25/g, "%");
            if (o.getHashByUrl(r)) throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");
            if (i !== !1 && o.busy()) return o.pushQueue({
                scope: o,
                callback: o.replaceState,
                args: arguments,
                queue: i
            }), !1;
            o.busy(!0);
            var s = o.createStateObject(t, n, r),
                u = o.getHashByState(s),
                a = o.getState(!1),
                f = o.getHashByState(a),
                l = o.getStateByIndex(-2);
            return o.discardState(a, s, l), u === f ? (o.storeState(s), o.expectedStateId = s.id, o.recycleState(s), o.setTitle(s), o.saveState(s), o.Adapter.trigger(e, "statechange"), o.busy(!1)) : o.pushState(s.data, s.title, s.url, !1), !0
        }), o.emulated.pushState && o.getHash() && !o.emulated.hashChange && o.Adapter.onDomLoad(function () {
            o.Adapter.trigger(e, "hashchange")
        })
    }, typeof o.init != "undefined" && o.init()
}(window),
function (e, t) {
    "use strict";
    var n = e.console || t,
        r = e.document,
        i = e.navigator,
        s = e.sessionStorage || !1,
        o = e.setTimeout,
        u = e.clearTimeout,
        a = e.setInterval,
        f = e.clearInterval,
        l = e.JSON,
        c = e.alert,
        h = e.History = e.History || {}, p = e.history;
    try {
        s.setItem("TEST", "1"), s.removeItem("TEST")
    } catch (d) {
        s = !1
    }
    l.stringify = l.stringify || l.encode, l.parse = l.parse || l.decode;
    if (typeof h.init != "undefined") throw new Error("History.js Core has already been loaded...");
    h.init = function (e) {
        return typeof h.Adapter == "undefined" ? !1 : (typeof h.initCore != "undefined" && h.initCore(), typeof h.initHtml4 != "undefined" && h.initHtml4(), !0)
    }, h.initCore = function (d) {
        if (typeof h.initCore.initialized != "undefined") return !1;
        h.initCore.initialized = !0, h.options = h.options || {}, h.options.hashChangeInterval = h.options.hashChangeInterval || 100, h.options.safariPollInterval = h.options.safariPollInterval || 500, h.options.doubleCheckInterval = h.options.doubleCheckInterval || 500, h.options.disableSuid = h.options.disableSuid || !1, h.options.storeInterval = h.options.storeInterval || 1e3, h.options.busyDelay = h.options.busyDelay || 250, h.options.debug = h.options.debug || !1, h.options.initialTitle = h.options.initialTitle || r.title, h.options.html4Mode = h.options.html4Mode || !1, h.options.delayInit = h.options.delayInit || !1, h.intervalList = [], h.clearAllIntervals = function () {
            var e, t = h.intervalList;
            if (typeof t != "undefined" && t !== null) {
                for (e = 0; e < t.length; e++) f(t[e]);
                h.intervalList = null
            }
        }, h.debug = function () {
            (h.options.debug || !1) && h.log.apply(h, arguments)
        }, h.log = function () {
            var e = typeof n != "undefined" && typeof n.log != "undefined" && typeof n.log.apply != "undefined",
                t = r.getElementById("log"),
                i, s, o, u, a;
            e ? (u = Array.prototype.slice.call(arguments), i = u.shift(), typeof n.debug != "undefined" ? n.debug.apply(n, [i, u]) : n.log.apply(n, [i, u])) : i = "\n" + arguments[0] + "\n";
            for (s = 1, o = arguments.length; s < o; ++s) {
                a = arguments[s];
                if (typeof a == "object" && typeof l != "undefined") try {
                    a = l.stringify(a)
                } catch (f) {}
                i += "\n" + a + "\n"
            }
            return t ? (t.value += i + "\n-----\n", t.scrollTop = t.scrollHeight - t.clientHeight) : e || c(i), !0
        }, h.getInternetExplorerMajorVersion = function () {
            var e = h.getInternetExplorerMajorVersion.cached = typeof h.getInternetExplorerMajorVersion.cached != "undefined" ? h.getInternetExplorerMajorVersion.cached : function () {
                    var e = 3,
                        t = r.createElement("div"),
                        n = t.getElementsByTagName("i");
                    while ((t.innerHTML = "<!--[if gt IE " + ++e + "]><i></i><![endif]-->") && n[0]);
                    return e > 4 ? e : !1
                }();
            return e
        }, h.isInternetExplorer = function () {
            var e = h.isInternetExplorer.cached = typeof h.isInternetExplorer.cached != "undefined" ? h.isInternetExplorer.cached : Boolean(h.getInternetExplorerMajorVersion());
            return e
        }, h.options.html4Mode ? h.emulated = {
            pushState: !0,
            hashChange: !0
        } : h.emulated = {
            pushState: !Boolean(e.history && e.history.pushState && e.history.replaceState && !/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(i.userAgent) && !/AppleWebKit\/5([0-2]|3[0-2])/i.test(i.userAgent)),
            hashChange: Boolean(!("onhashchange" in e || "onhashchange" in r) || h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8)
        }, h.enabled = !h.emulated.pushState, h.bugs = {
            setHash: Boolean(!h.emulated.pushState && i.vendor === "Apple Computer, Inc." && /AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),
            safariPoll: Boolean(!h.emulated.pushState && i.vendor === "Apple Computer, Inc." && /AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)),
            ieDoubleCheck: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8),
            hashEscape: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 7)
        }, h.isEmptyObject = function (e) {
            for (var t in e)
                if (e.hasOwnProperty(t)) return !1;
            return !0
        }, h.cloneObject = function (e) {
            var t, n;
            return e ? (t = l.stringify(e), n = l.parse(t)) : n = {}, n
        }, h.getRootUrl = function () {
            var e = r.location.protocol + "//" + (r.location.hostname || r.location.host);
            if (r.location.port || !1) e += ":" + r.location.port;
            return e += "/", e
        }, h.getBaseHref = function () {
            var e = r.getElementsByTagName("base"),
                t = null,
                n = "";
            return e.length === 1 && (t = e[0], n = t.href.replace(/[^\/]+$/, "")), n = n.replace(/\/+$/, ""), n && (n += "/"), n
        }, h.getBaseUrl = function () {
            var e = h.getBaseHref() || h.getBasePageUrl() || h.getRootUrl();
            return e
        }, h.getPageUrl = function () {
            var e = h.getState(!1, !1),
                t = (e || {}).url || h.getLocationHref(),
                n;
            return n = t.replace(/\/+$/, "").replace(/[^\/]+$/, function (e, t, n) {
                return /\./.test(e) ? e : e + "/"
            }), n
        }, h.getBasePageUrl = function () {
            var e = h.getLocationHref().replace(/[#\?].*/, "").replace(/[^\/]+$/, function (e, t, n) {
                return /[^\/]$/.test(e) ? "" : e
            }).replace(/\/+$/, "") + "/";
            return e
        }, h.getFullUrl = function (e, t) {
            var n = e,
                r = e.substring(0, 1);
            return t = typeof t == "undefined" ? !0 : t, /[a-z]+\:\/\//.test(e) || (r === "/" ? n = h.getRootUrl() + e.replace(/^\/+/, "") : r === "#" ? n = h.getPageUrl().replace(/#.*/, "") + e : r === "?" ? n = h.getPageUrl().replace(/[\?#].*/, "") + e : t ? n = h.getBaseUrl() + e.replace(/^(\.\/)+/, "") : n = h.getBasePageUrl() + e.replace(/^(\.\/)+/, "")), n.replace(/\#$/, "")
        }, h.getShortUrl = function (e) {
            var t = e,
                n = h.getBaseUrl(),
                r = h.getRootUrl();
            return h.emulated.pushState && (t = t.replace(n, "")), t = t.replace(r, "/"), h.isTraditionalAnchor(t) && (t = "./" + t), t = t.replace(/^(\.\/)+/g, "./").replace(/\#$/, ""), t
        }, h.getLocationHref = function (e) {
            return e = e || r, e.URL === e.location.href ? e.location.href : e.location.href === decodeURIComponent(e.URL) ? e.URL : e.location.hash && decodeURIComponent(e.location.href.replace(/^[^#]+/, "")) === e.location.hash ? e.location.href : e.URL.indexOf("#") == -1 && e.location.href.indexOf("#") != -1 ? e.location.href : e.URL || e.location.href
        }, h.store = {}, h.idToState = h.idToState || {}, h.stateToId = h.stateToId || {}, h.urlToId = h.urlToId || {}, h.storedStates = h.storedStates || [], h.savedStates = h.savedStates || [], h.normalizeStore = function () {
            h.store.idToState = h.store.idToState || {}, h.store.urlToId = h.store.urlToId || {}, h.store.stateToId = h.store.stateToId || {}
        }, h.getState = function (e, t) {
            typeof e == "undefined" && (e = !0), typeof t == "undefined" && (t = !0);
            var n = h.getLastSavedState();
            return !n && t && (n = h.createStateObject()), e && (n = h.cloneObject(n), n.url = n.cleanUrl || n.url), n
        }, h.getIdByState = function (e) {
            var t = h.extractId(e.url),
                n;
            if (!t) {
                n = h.getStateString(e);
                if (typeof h.stateToId[n] != "undefined") t = h.stateToId[n];
                else if (typeof h.store.stateToId[n] != "undefined") t = h.store.stateToId[n];
                else {
                    for (;;) {
                        t = (new Date).getTime() + String(Math.random()).replace(/\D/g, "");
                        if (typeof h.idToState[t] == "undefined" && typeof h.store.idToState[t] == "undefined") break
                    }
                    h.stateToId[n] = t, h.idToState[t] = e
                }
            }
            return t
        }, h.normalizeState = function (e) {
            var t, n;
            if (!e || typeof e != "object") e = {};
            if (typeof e.normalized != "undefined") return e;
            if (!e.data || typeof e.data != "object") e.data = {};
            return t = {}, t.normalized = !0, t.title = e.title || "", t.url = h.getFullUrl(e.url ? e.url : h.getLocationHref()), t.hash = h.getShortUrl(t.url), t.data = h.cloneObject(e.data), t.id = h.getIdByState(t), t.cleanUrl = t.url.replace(/\??\&_suid.*/, ""), t.url = t.cleanUrl, n = !h.isEmptyObject(t.data), (t.title || n) && h.options.disableSuid !== !0 && (t.hash = h.getShortUrl(t.url).replace(/\??\&_suid.*/, ""), /\?/.test(t.hash) || (t.hash += "?"), t.hash += "&_suid=" + t.id), t.hashedUrl = h.getFullUrl(t.hash), (h.emulated.pushState || h.bugs.safariPoll) && h.hasUrlDuplicate(t) && (t.url = t.hashedUrl), t
        }, h.createStateObject = function (e, t, n) {
            var r = {
                data: e,
                title: t,
                url: n
            };
            return r = h.normalizeState(r), r
        }, h.getStateById = function (e) {
            e = String(e);
            var n = h.idToState[e] || h.store.idToState[e] || t;
            return n
        }, h.getStateString = function (e) {
            var t, n, r;
            return t = h.normalizeState(e), n = {
                data: t.data,
                title: e.title,
                url: e.url
            }, r = l.stringify(n), r
        }, h.getStateId = function (e) {
            var t, n;
            return t = h.normalizeState(e), n = t.id, n
        }, h.getHashByState = function (e) {
            var t, n;
            return t = h.normalizeState(e), n = t.hash, n
        }, h.extractId = function (e) {
            var t, n, r, i;
            return e.indexOf("#") != -1 ? i = e.split("#")[0] : i = e, n = /(.*)\&_suid=([0-9]+)$/.exec(i), r = n ? n[1] || e : e, t = n ? String(n[2] || "") : "", t || !1
        }, h.isTraditionalAnchor = function (e) {
            var t = !/[\/\?\.]/.test(e);
            return t
        }, h.extractState = function (e, t) {
            var n = null,
                r, i;
            return t = t || !1, r = h.extractId(e), r && (n = h.getStateById(r)), n || (i = h.getFullUrl(e), r = h.getIdByUrl(i) || !1, r && (n = h.getStateById(r)), !n && t && !h.isTraditionalAnchor(e) && (n = h.createStateObject(null, null, i))), n
        }, h.getIdByUrl = function (e) {
            var n = h.urlToId[e] || h.store.urlToId[e] || t;
            return n
        }, h.getLastSavedState = function () {
            return h.savedStates[h.savedStates.length - 1] || t
        }, h.getLastStoredState = function () {
            return h.storedStates[h.storedStates.length - 1] || t
        }, h.hasUrlDuplicate = function (e) {
            var t = !1,
                n;
            return n = h.extractState(e.url), t = n && n.id !== e.id, t
        }, h.storeState = function (e) {
            return h.urlToId[e.url] = e.id, h.storedStates.push(h.cloneObject(e)), e
        }, h.isLastSavedState = function (e) {
            var t = !1,
                n, r, i;
            return h.savedStates.length && (n = e.id, r = h.getLastSavedState(), i = r.id, t = n === i), t
        }, h.saveState = function (e) {
            return h.isLastSavedState(e) ? !1 : (h.savedStates.push(h.cloneObject(e)), !0)
        }, h.getStateByIndex = function (e) {
            var t = null;
            return typeof e == "undefined" ? t = h.savedStates[h.savedStates.length - 1] : e < 0 ? t = h.savedStates[h.savedStates.length + e] : t = h.savedStates[e], t
        }, h.getCurrentIndex = function () {
            var e = null;
            return h.savedStates.length < 1 ? e = 0 : e = h.savedStates.length - 1, e
        }, h.getHash = function (e) {
            var t = h.getLocationHref(e),
                n;
            return n = h.getHashByUrl(t), n
        }, h.unescapeHash = function (e) {
            var t = h.normalizeHash(e);
            return t = decodeURIComponent(t), t
        }, h.normalizeHash = function (e) {
            var t = e.replace(/[^#]*#/, "").replace(/#.*/, "");
            return t
        }, h.setHash = function (e, t) {
            var n, i;
            return t !== !1 && h.busy() ? (h.pushQueue({
                scope: h,
                callback: h.setHash,
                args: arguments,
                queue: t
            }), !1) : (h.busy(!0), n = h.extractState(e, !0), n && !h.emulated.pushState ? h.pushState(n.data, n.title, n.url, !1) : h.getHash() !== e && (h.bugs.setHash ? (i = h.getPageUrl(), h.pushState(null, null, i + "#" + e, !1)) : r.location.hash = e), h)
        }, h.escapeHash = function (t) {
            var n = h.normalizeHash(t);
            return n = e.encodeURIComponent(n), h.bugs.hashEscape || (n = n.replace(/\%21/g, "!").replace(/\%26/g, "&").replace(/\%3D/g, "=").replace(/\%3F/g, "?")), n
        }, h.getHashByUrl = function (e) {
            var t = String(e).replace(/([^#]*)#?([^#]*)#?(.*)/, "$2");
            return t = h.unescapeHash(t), t
        }, h.setTitle = function (e) {
            var t = e.title,
                n;
            t || (n = h.getStateByIndex(0), n && n.url === e.url && (t = n.title || h.options.initialTitle));
            try {
                r.getElementsByTagName("title")[0].innerHTML = t.replace("<", "&lt;").replace(">", "&gt;").replace(" & ", " &amp; ")
            } catch (i) {}
            return r.title = t, h
        }, h.queues = [], h.busy = function (e) {
            typeof e != "undefined" ? h.busy.flag = e : typeof h.busy.flag == "undefined" && (h.busy.flag = !1);
            if (!h.busy.flag) {
                u(h.busy.timeout);
                var t = function () {
                    var e, n, r;
                    if (h.busy.flag) return;
                    for (e = h.queues.length - 1; e >= 0; --e) {
                        n = h.queues[e];
                        if (n.length === 0) continue;
                        r = n.shift(), h.fireQueueItem(r), h.busy.timeout = o(t, h.options.busyDelay)
                    }
                };
                h.busy.timeout = o(t, h.options.busyDelay)
            }
            return h.busy.flag
        }, h.busy.flag = !1, h.fireQueueItem = function (e) {
            return e.callback.apply(e.scope || h, e.args || [])
        }, h.pushQueue = function (e) {
            return h.queues[e.queue || 0] = h.queues[e.queue || 0] || [], h.queues[e.queue || 0].push(e), h
        }, h.queue = function (e, t) {
            return typeof e == "function" && (e = {
                callback: e
            }), typeof t != "undefined" && (e.queue = t), h.busy() ? h.pushQueue(e) : h.fireQueueItem(e), h
        }, h.clearQueue = function () {
            return h.busy.flag = !1, h.queues = [], h
        }, h.stateChanged = !1, h.doubleChecker = !1, h.doubleCheckComplete = function () {
            return h.stateChanged = !0, h.doubleCheckClear(), h
        }, h.doubleCheckClear = function () {
            return h.doubleChecker && (u(h.doubleChecker), h.doubleChecker = !1), h
        }, h.doubleCheck = function (e) {
            return h.stateChanged = !1, h.doubleCheckClear(), h.bugs.ieDoubleCheck && (h.doubleChecker = o(function () {
                return h.doubleCheckClear(), h.stateChanged || e(), !0
            }, h.options.doubleCheckInterval)), h
        }, h.safariStatePoll = function () {
            var t = h.extractState(h.getLocationHref()),
                n;
            if (!h.isLastSavedState(t)) return n = t, n || (n = h.createStateObject()), h.Adapter.trigger(e, "popstate"), h;
            return
        }, h.back = function (e) {
            return e !== !1 && h.busy() ? (h.pushQueue({
                scope: h,
                callback: h.back,
                args: arguments,
                queue: e
            }), !1) : (h.busy(!0), h.doubleCheck(function () {
                h.back(!1)
            }), p.go(-1), !0)
        }, h.forward = function (e) {
            return e !== !1 && h.busy() ? (h.pushQueue({
                scope: h,
                callback: h.forward,
                args: arguments,
                queue: e
            }), !1) : (h.busy(!0), h.doubleCheck(function () {
                h.forward(!1)
            }), p.go(1), !0)
        }, h.go = function (e, t) {
            var n;
            if (e > 0)
                for (n = 1; n <= e; ++n) h.forward(t);
            else {
                if (!(e < 0)) throw new Error("History.go: History.go requires a positive or negative integer passed.");
                for (n = -1; n >= e; --n) h.back(t)
            }
            return h
        };
        if (h.emulated.pushState) {
            var v = function () {};
            h.pushState = h.pushState || v, h.replaceState = h.replaceState || v
        } else h.onPopState = function (t, n) {
            var r = !1,
                i = !1,
                s, o;
            return h.doubleCheckComplete(), s = h.getHash(), s ? (o = h.extractState(s || h.getLocationHref(), !0), o ? h.replaceState(o.data, o.title, o.url, !1) : (h.Adapter.trigger(e, "anchorchange"), h.busy(!1)), h.expectedStateId = !1, !1) : (r = h.Adapter.extractEventData("state", t, n) || !1, r ? i = h.getStateById(r) : h.expectedStateId ? i = h.getStateById(h.expectedStateId) : i = h.extractState(h.getLocationHref()), i || (i = h.createStateObject(null, null, h.getLocationHref())), h.expectedStateId = !1, h.isLastSavedState(i) ? (h.busy(!1), !1) : (h.storeState(i), h.saveState(i), h.setTitle(i), h.Adapter.trigger(e, "statechange"), h.busy(!1), !0))
        }, h.Adapter.bind(e, "popstate", h.onPopState), h.pushState = function (t, n, r, i) {
            if (h.getHashByUrl(r) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
            if (i !== !1 && h.busy()) return h.pushQueue({
                scope: h,
                callback: h.pushState,
                args: arguments,
                queue: i
            }), !1;
            h.busy(!0);
            var s = h.createStateObject(t, n, r);
            return h.isLastSavedState(s) ? h.busy(!1) : (h.storeState(s), h.expectedStateId = s.id, p.pushState(s.id, s.title, s.url), h.Adapter.trigger(e, "popstate")), !0
        }, h.replaceState = function (t, n, r, i) {
            if (h.getHashByUrl(r) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
            if (i !== !1 && h.busy()) return h.pushQueue({
                scope: h,
                callback: h.replaceState,
                args: arguments,
                queue: i
            }), !1;
            h.busy(!0);
            var s = h.createStateObject(t, n, r);
            return h.isLastSavedState(s) ? h.busy(!1) : (h.storeState(s), h.expectedStateId = s.id, p.replaceState(s.id, s.title, s.url), h.Adapter.trigger(e, "popstate")), !0
        }; if (s) {
            try {
                h.store = l.parse(s.getItem("History.store")) || {}
            } catch (m) {
                h.store = {}
            }
            h.normalizeStore()
        } else h.store = {}, h.normalizeStore();
        h.Adapter.bind(e, "unload", h.clearAllIntervals), h.saveState(h.storeState(h.extractState(h.getLocationHref(), !0))), s && (h.onUnload = function () {
            var e, t, n;
            try {
                e = l.parse(s.getItem("History.store")) || {}
            } catch (r) {
                e = {}
            }
            e.idToState = e.idToState || {}, e.urlToId = e.urlToId || {}, e.stateToId = e.stateToId || {};
            for (t in h.idToState) {
                if (!h.idToState.hasOwnProperty(t)) continue;
                e.idToState[t] = h.idToState[t]
            }
            for (t in h.urlToId) {
                if (!h.urlToId.hasOwnProperty(t)) continue;
                e.urlToId[t] = h.urlToId[t]
            }
            for (t in h.stateToId) {
                if (!h.stateToId.hasOwnProperty(t)) continue;
                e.stateToId[t] = h.stateToId[t]
            }
            h.store = e, h.normalizeStore(), n = l.stringify(e);
            try {
                s.setItem("History.store", n)
            } catch (i) {
                if (i.code !== DOMException.QUOTA_EXCEEDED_ERR) throw i;
                s.length && (s.removeItem("History.store"), s.setItem("History.store", n))
            }
        }, h.intervalList.push(a(h.onUnload, h.options.storeInterval)), h.Adapter.bind(e, "beforeunload", h.onUnload), h.Adapter.bind(e, "unload", h.onUnload));
        if (!h.emulated.pushState) {
            h.bugs.safariPoll && h.intervalList.push(a(h.safariStatePoll, h.options.safariPollInterval));
            if (i.vendor === "Apple Computer, Inc." || (i.appCodeName || "") === "Mozilla") h.Adapter.bind(e, "hashchange", function () {
                h.Adapter.trigger(e, "popstate")
            }), h.getHash() && h.Adapter.onDomLoad(function () {
                h.Adapter.trigger(e, "hashchange")
            })
        }
    }, (!h.options || !h.options.delayInit) && h.init()
}(window);;
(function (window, $, undefined) {
    "use strict";
    $.infinitescroll = function infscr(options, callback, element) {
        this.element = $(element);
        if (!this._create(options, callback)) {
            this.failed = true;
        }
    };
    $.infinitescroll.defaults = {
        loading: {
            finished: undefined,
            finishedMsg: "<em>Congratulations, you've reached the end of the internet.</em>",
            img: "data:image/gif;base64,R0lGODlhAQABAHAAACH5BAUKAAAALAAAAAABAAEAQAICRAEAOw==",
            msg: null,
            msgText: "<em>Loading the next set of posts...</em>",
            selector: null,
            speed: 'fast',
            start: undefined
        },
        state: {
            isDuringAjax: false,
            isInvalidPage: false,
            isDestroyed: false,
            isDone: false,
            isPaused: false,
            isBeyondMaxPage: false,
            currPage: 1
        },
        debug: false,
        behavior: undefined,
        binder: $(window),
        nextSelector: "div.navigation a:first",
        navSelector: "div.navigation",
        contentSelector: null,
        extraScrollPx: 150,
        itemSelector: "div.post",
        animate: false,
        pathParse: undefined,
        dataType: 'html',
        appendCallback: true,
        bufferPx: 40,
        errorCallback: function () {},
        infid: 0,
        pixelsFromNavToBottom: undefined,
        path: undefined,
        prefill: false,
        maxPage: undefined
    };
    $.infinitescroll.prototype = {
        _binding: function infscr_binding(binding) {
            var instance = this,
                opts = instance.options;
            opts.v = '2.0b2.120520';
            if ( !! opts.behavior && this['_binding_' + opts.behavior] !== undefined) {
                this['_binding_' + opts.behavior].call(this);
                return;
            }
            if (binding !== 'bind' && binding !== 'unbind') {
                this._debug('Binding value  ' + binding + ' not valid');
                return false;
            }
            if (binding === 'unbind') {
                (this.options.binder).unbind('smartscroll.infscr.' + instance.options.infid);
            } else {
                (this.options.binder)[binding]('smartscroll.infscr.' + instance.options.infid, function () {
                    instance.scroll();
                });
            }
            this._debug('Binding', binding);
        },
        _create: function infscr_create(options, callback) {
            var opts = $.extend(true, {}, $.infinitescroll.defaults, options);
            this.options = opts;
            var $window = $(window);
            var instance = this;
            if (!instance._validate(options)) {
                return false;
            }
            var path = $(opts.nextSelector).attr('href');
            if (!path) {
                this._debug('Navigation selector not found');
                return false;
            }
            opts.path = opts.path || this._determinepath(path);
            opts.contentSelector = opts.contentSelector || this.element;
            opts.loading.selector = opts.loading.selector || opts.contentSelector;
            opts.loading.msg = opts.loading.msg || $('<div id="infscr-loading"><img alt="Loading..." src="' + opts.loading.img + '" /><div>' + opts.loading.msgText + '</div></div>');
            (new Image()).src = opts.loading.img;
            if (opts.pixelsFromNavToBottom === undefined) {
                opts.pixelsFromNavToBottom = $(document).height() - $(opts.navSelector).offset().top;
                this._debug("pixelsFromNavToBottom: " + opts.pixelsFromNavToBottom);
            }
            var self = this;
            opts.loading.start = opts.loading.start || function () {
                $(opts.navSelector).hide();
                opts.loading.msg.appendTo(opts.loading.selector).show(opts.loading.speed, $.proxy(function () {
                    this.beginAjax(opts);
                }, self));
            };
            opts.loading.finished = opts.loading.finished || function () {
                if (!opts.state.isBeyondMaxPage)
                    opts.loading.msg.fadeOut(opts.loading.speed);
            };
            opts.callback = function (instance, data, url) {
                if ( !! opts.behavior && instance['_callback_' + opts.behavior] !== undefined) {
                    instance['_callback_' + opts.behavior].call($(opts.contentSelector)[0], data, url);
                }
                if (callback) {
                    callback.call($(opts.contentSelector)[0], data, opts, url);
                }
                if (opts.prefill) {
                    $window.bind("resize.infinite-scroll", instance._prefill);
                }
            };
            if (options.debug) {
                if (Function.prototype.bind && (typeof console === 'object' || typeof console === 'function') && typeof console.log === "object") {
                    ["log", "info", "warn", "error", "assert", "dir", "clear", "profile", "profileEnd"].forEach(function (method) {
                        console[method] = this.call(console[method], console);
                    }, Function.prototype.bind);
                }
            }
            this._setup();
            if (opts.prefill) {
                this._prefill();
            }
            return true;
        },
        _prefill: function infscr_prefill() {
            var instance = this;
            var $window = $(window);

            function needsPrefill() {
                return (instance.options.contentSelector.height() <= $window.height());
            }
            this._prefill = function () {
                if (needsPrefill()) {
                    instance.scroll();
                }
                $window.bind("resize.infinite-scroll", function () {
                    if (needsPrefill()) {
                        $window.unbind("resize.infinite-scroll");
                        instance.scroll();
                    }
                });
            };
            this._prefill();
        },
        _debug: function infscr_debug() {
            if (true !== this.options.debug) {
                return;
            }
            if (typeof console !== 'undefined' && typeof console.log === 'function') {
                if ((Array.prototype.slice.call(arguments)).length === 1 && typeof Array.prototype.slice.call(arguments)[0] === 'string') {
                    console.log((Array.prototype.slice.call(arguments)).toString());
                } else {
                    console.log(Array.prototype.slice.call(arguments));
                }
            } else if (!Function.prototype.bind && typeof console !== 'undefined' && typeof console.log === 'object') {
                Function.prototype.call.call(console.log, console, Array.prototype.slice.call(arguments));
            }
        },
        _determinepath: function infscr_determinepath(path) {
            var opts = this.options;
            if ( !! opts.behavior && this['_determinepath_' + opts.behavior] !== undefined) {
                return this['_determinepath_' + opts.behavior].call(this, path);
            }
            if ( !! opts.pathParse) {
                this._debug('pathParse manual');
                return opts.pathParse(path, this.options.state.currPage + 1);
            } else if (path.match(/^(.*?)\b2\b(.*?$)/)) {
                path = path.match(/^(.*?)\b2\b(.*?$)/).slice(1);
            } else if (path.match(/^(.*?)2(.*?$)/)) {
                if (path.match(/^(.*?page=)2(\/.*|$)/)) {
                    path = path.match(/^(.*?page=)2(\/.*|$)/).slice(1);
                    return path;
                }
                path = path.match(/^(.*?)2(.*?$)/).slice(1);
            } else {
                if (path.match(/^(.*?page=)1(\/.*|$)/)) {
                    path = path.match(/^(.*?page=)1(\/.*|$)/).slice(1);
                    return path;
                } else {
                    this._debug('Sorry, we couldn\'t parse your Next (Previous Posts) URL. Verify your the css selector points to the correct A tag. If you still get this error: yell, scream, and kindly ask for help at infinite-scroll.com.');
                    opts.state.isInvalidPage = true;
                }
            }
            this._debug('determinePath', path);
            return path;
        },
        _error: function infscr_error(xhr) {
            var opts = this.options;
            if ( !! opts.behavior && this['_error_' + opts.behavior] !== undefined) {
                this['_error_' + opts.behavior].call(this, xhr);
                return;
            }
            if (xhr !== 'destroy' && xhr !== 'end') {
                xhr = 'unknown';
            }
            this._debug('Error', xhr);
            if (xhr === 'end' || opts.state.isBeyondMaxPage) {
                this._showdonemsg();
            }
            opts.state.isDone = true;
            opts.state.currPage = 1;
            opts.state.isPaused = false;
            opts.state.isBeyondMaxPage = false;
            this._binding('unbind');
        },
        _loadcallback: function infscr_loadcallback(box, data, url) {
            var opts = this.options,
                callback = this.options.callback,
                result = (opts.state.isDone) ? 'done' : (!opts.appendCallback) ? 'no-append' : 'append',
                frag;
            if ( !! opts.behavior && this['_loadcallback_' + opts.behavior] !== undefined) {
                this['_loadcallback_' + opts.behavior].call(this, box, data);
                return;
            }
            switch (result) {
            case 'done':
                this._showdonemsg();
                return false;
            case 'no-append':
                if (opts.dataType === 'html') {
                    data = '<div>' + data + '</div>';
                    data = $(data).find(opts.itemSelector);
                }
                break;
            case 'append':
                var children = box.children();
                if (children.length === 0) {
                    return this._error('end');
                }
                frag = document.createDocumentFragment();
                while (box[0].firstChild) {
                    frag.appendChild(box[0].firstChild);
                }
                this._debug('contentSelector', $(opts.contentSelector)[0]);
                $(opts.contentSelector)[0].appendChild(frag);
                data = children.get();
                break;
            }
            opts.loading.finished.call($(opts.contentSelector)[0], opts);
            if (opts.animate) {
                var scrollTo = $(window).scrollTop() + $(opts.loading.msg).height() + opts.extraScrollPx + 'px';
                $('html,body').animate({
                    scrollTop: scrollTo
                }, 800, function () {
                    opts.state.isDuringAjax = false;
                });
            }
            if (!opts.animate) {
                opts.state.isDuringAjax = false;
            }
            callback(this, data, url);
            if (opts.prefill) {
                this._prefill();
            }
        },
        _nearbottom: function infscr_nearbottom() {
            var opts = this.options,
                pixelsFromWindowBottomToBottom = 0 + $(document).height() - (opts.binder.scrollTop()) - $(window).height();
            if ( !! opts.behavior && this['_nearbottom_' + opts.behavior] !== undefined) {
                return this['_nearbottom_' + opts.behavior].call(this);
            }
            this._debug('math:', pixelsFromWindowBottomToBottom, opts.pixelsFromNavToBottom);
            return (pixelsFromWindowBottomToBottom - opts.bufferPx < opts.pixelsFromNavToBottom);
        },
        _pausing: function infscr_pausing(pause) {
            var opts = this.options;
            if ( !! opts.behavior && this['_pausing_' + opts.behavior] !== undefined) {
                this['_pausing_' + opts.behavior].call(this, pause);
                return;
            }
            if (pause !== 'pause' && pause !== 'resume' && pause !== null) {
                this._debug('Invalid argument. Toggling pause value instead');
            }
            pause = (pause && (pause === 'pause' || pause === 'resume')) ? pause : 'toggle';
            switch (pause) {
            case 'pause':
                opts.state.isPaused = true;
                break;
            case 'resume':
                opts.state.isPaused = false;
                break;
            case 'toggle':
                opts.state.isPaused = !opts.state.isPaused;
                break;
            }
            this._debug('Paused', opts.state.isPaused);
            return false;
        },
        _setup: function infscr_setup() {
            var opts = this.options;
            if ( !! opts.behavior && this['_setup_' + opts.behavior] !== undefined) {
                this['_setup_' + opts.behavior].call(this);
                return;
            }
            this._binding('bind');
            return false;
        },
        _showdonemsg: function infscr_showdonemsg() {
            var opts = this.options;
            if ( !! opts.behavior && this['_showdonemsg_' + opts.behavior] !== undefined) {
                this['_showdonemsg_' + opts.behavior].call(this);
                return;
            }
            opts.loading.msg.find('img').hide().parent().find('div').html(opts.loading.finishedMsg).animate({
                opacity: 1
            }, 2000, function () {
                $(this).parent().fadeOut(opts.loading.speed);
            });
            opts.errorCallback.call($(opts.contentSelector)[0], 'done');
        },
        _validate: function infscr_validate(opts) {
            for (var key in opts) {
                if (key.indexOf && key.indexOf('Selector') > -1 && $(opts[key]).length === 0) {
                    this._debug('Your ' + key + ' found no elements.');
                    return false;
                }
            }
            return true;
        },
        bind: function infscr_bind() {
            this._binding('bind');
        },
        destroy: function infscr_destroy() {
            this.options.state.isDestroyed = true;
            this.options.loading.finished();
            return this._error('destroy');
        },
        pause: function infscr_pause() {
            this._pausing('pause');
        },
        resume: function infscr_resume() {
            this._pausing('resume');
        },
        beginAjax: function infscr_ajax(opts) {
            var instance = this,
                path = opts.path,
                box, desturl, method, condition;
            opts.state.currPage++;
            if (opts.maxPage != undefined && opts.state.currPage > opts.maxPage) {
                opts.state.isBeyondMaxPage = true;
                this.destroy();
                return;
            }
            box = $(opts.contentSelector).is('table, tbody') ? $('<tbody/>') : $('<div/>');
            desturl = (typeof path === 'function') ? path(opts.state.currPage) : path.join(opts.state.currPage);
            instance._debug('heading into ajax', desturl);
            method = (opts.dataType === 'html' || opts.dataType === 'json') ? opts.dataType : 'html+callback';
            if (opts.appendCallback && opts.dataType === 'html') {
                method += '+callback';
            }
            switch (method) {
            case 'html+callback':
                instance._debug('Using HTML via .load() method');
                box.load(desturl + ' ' + opts.itemSelector, undefined, function infscr_ajax_callback(responseText) {
                    instance._loadcallback(box, responseText, desturl);
                });
                break;
            case 'html':
                instance._debug('Using ' + (method.toUpperCase()) + ' via $.ajax() method');
                $.ajax({
                    url: desturl,
                    dataType: opts.dataType,
                    complete: function infscr_ajax_callback(jqXHR, textStatus) {
                        condition = (typeof (jqXHR.isResolved) !== 'undefined') ? (jqXHR.isResolved()) : (textStatus === "success" || textStatus === "notmodified");
                        if (condition) {
                            instance._loadcallback(box, jqXHR.responseText, desturl);
                        } else {
                            instance._error('end');
                        }
                    }
                });
                break;
            case 'json':
                instance._debug('Using ' + (method.toUpperCase()) + ' via $.ajax() method');
                $.ajax({
                    dataType: 'json',
                    type: 'GET',
                    url: desturl,
                    success: function (data, textStatus, jqXHR) {
                        condition = (typeof (jqXHR.isResolved) !== 'undefined') ? (jqXHR.isResolved()) : (textStatus === "success" || textStatus === "notmodified");
                        if (opts.appendCallback) {
                            if (opts.template !== undefined) {
                                var theData = opts.template(data);
                                box.append(theData);
                                if (condition) {
                                    instance._loadcallback(box, theData);
                                } else {
                                    instance._error('end');
                                }
                            } else {
                                instance._debug("template must be defined.");
                                instance._error('end');
                            }
                        } else {
                            if (condition) {
                                instance._loadcallback(box, data, desturl);
                            } else {
                                instance._error('end');
                            }
                        }
                    },
                    error: function () {
                        instance._debug("JSON ajax request failed.");
                        instance._error('end');
                    }
                });
                break;
            }
        },
        retrieve: function infscr_retrieve(pageNum) {
            pageNum = pageNum || null;
            var instance = this,
                opts = instance.options;
            if ( !! opts.behavior && this['retrieve_' + opts.behavior] !== undefined) {
                this['retrieve_' + opts.behavior].call(this, pageNum);
                return;
            }
            if (opts.state.isDestroyed) {
                this._debug('Instance is destroyed');
                return false;
            }
            opts.state.isDuringAjax = true;
            opts.loading.start.call($(opts.contentSelector)[0], opts);
        },
        scroll: function infscr_scroll() {
            var opts = this.options,
                state = opts.state;
            if ( !! opts.behavior && this['scroll_' + opts.behavior] !== undefined) {
                this['scroll_' + opts.behavior].call(this);
                return;
            }
            if (state.isDuringAjax || state.isInvalidPage || state.isDone || state.isDestroyed || state.isPaused) {
                return;
            }
            if (!this._nearbottom()) {
                return;
            }
            this.retrieve();
        },
        toggle: function infscr_toggle() {
            this._pausing();
        },
        unbind: function infscr_unbind() {
            this._binding('unbind');
        },
        update: function infscr_options(key) {
            if ($.isPlainObject(key)) {
                this.options = $.extend(true, this.options, key);
            }
        }
    };
    $.fn.infinitescroll = function infscr_init(options, callback) {
        var thisCall = typeof options;
        switch (thisCall) {
        case 'string':
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function () {
                var instance = $.data(this, 'infinitescroll');
                if (!instance) {
                    return false;
                }
                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                    return false;
                }
                instance[options].apply(instance, args);
            });
            break;
        case 'object':
            this.each(function () {
                var instance = $.data(this, 'infinitescroll');
                if (instance) {
                    instance.update(options);
                } else {
                    instance = new $.infinitescroll(options, callback, this);
                    if (!instance.failed) {
                        $.data(this, 'infinitescroll', instance);
                    }
                }
            });
            break;
        }
        return this;
    };
    var event = $.event,
        scrollTimeout;
    event.special.smartscroll = {
        setup: function () {
            $(this).bind("scroll", event.special.smartscroll.handler);
        },
        teardown: function () {
            $(this).unbind("scroll", event.special.smartscroll.handler);
        },
        handler: function (event, execAsap) {
            var context = this,
                args = arguments;
            event.type = "smartscroll";
            if (scrollTimeout) {
                clearTimeout(scrollTimeout);
            }
            scrollTimeout = setTimeout(function () {
                $(context).trigger('smartscroll', args);
            }, execAsap === "execAsap" ? 0 : 100);
        }
    };
    $.fn.smartscroll = function (fn) {
        return fn ? this.bind("smartscroll", fn) : this.trigger("smartscroll", ["execAsap"]);
    };
})(window, jQuery);;
/*!
 * imagesLoaded PACKAGED v3.0.4
 * JavaScript is all like "You images are done yet or what?"
 */

(function () {
    "use strict";

    function e() {}

    function t(e, t) {
        for (var n = e.length; n--;)
            if (e[n].listener === t) return n;
        return -1
    }
    var n = e.prototype;
    n.getListeners = function (e) {
        var t, n, i = this._getEvents();
        if ("object" == typeof e) {
            t = {};
            for (n in i) i.hasOwnProperty(n) && e.test(n) && (t[n] = i[n])
        } else t = i[e] || (i[e] = []);
        return t
    }, n.flattenListeners = function (e) {
        var t, n = [];
        for (t = 0; e.length > t; t += 1) n.push(e[t].listener);
        return n
    }, n.getListenersAsObject = function (e) {
        var t, n = this.getListeners(e);
        return n instanceof Array && (t = {}, t[e] = n), t || n
    }, n.addListener = function (e, n) {
        var i, r = this.getListenersAsObject(e),
            o = "object" == typeof n;
        for (i in r) r.hasOwnProperty(i) && -1 === t(r[i], n) && r[i].push(o ? n : {
            listener: n,
            once: !1
        });
        return this
    }, n.on = n.addListener, n.addOnceListener = function (e, t) {
        return this.addListener(e, {
            listener: t,
            once: !0
        })
    }, n.once = n.addOnceListener, n.defineEvent = function (e) {
        return this.getListeners(e), this
    }, n.defineEvents = function (e) {
        for (var t = 0; e.length > t; t += 1) this.defineEvent(e[t]);
        return this
    }, n.removeListener = function (e, n) {
        var i, r, o = this.getListenersAsObject(e);
        for (r in o) o.hasOwnProperty(r) && (i = t(o[r], n), -1 !== i && o[r].splice(i, 1));
        return this
    }, n.off = n.removeListener, n.addListeners = function (e, t) {
        return this.manipulateListeners(!1, e, t)
    }, n.removeListeners = function (e, t) {
        return this.manipulateListeners(!0, e, t)
    }, n.manipulateListeners = function (e, t, n) {
        var i, r, o = e ? this.removeListener : this.addListener,
            s = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
            for (i = n.length; i--;) o.call(this, t, n[i]);
        else
            for (i in t) t.hasOwnProperty(i) && (r = t[i]) && ("function" == typeof r ? o.call(this, i, r) : s.call(this, i, r));
        return this
    }, n.removeEvent = function (e) {
        var t, n = typeof e,
            i = this._getEvents();
        if ("string" === n) delete i[e];
        else if ("object" === n)
            for (t in i) i.hasOwnProperty(t) && e.test(t) && delete i[t];
        else delete this._events;
        return this
    }, n.emitEvent = function (e, t) {
        var n, i, r, o, s = this.getListenersAsObject(e);
        for (r in s)
            if (s.hasOwnProperty(r))
                for (i = s[r].length; i--;) n = s[r][i], o = n.listener.apply(this, t || []), (o === this._getOnceReturnValue() || n.once === !0) && this.removeListener(e, s[r][i].listener);
        return this
    }, n.trigger = n.emitEvent, n.emit = function (e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }, n.setOnceReturnValue = function (e) {
        return this._onceReturnValue = e, this
    }, n._getOnceReturnValue = function () {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }, n._getEvents = function () {
        return this._events || (this._events = {})
    }, "function" == typeof define && define.amd ? define(function () {
        return e
    }) : "undefined" != typeof module && module.exports ? module.exports = e : this.EventEmitter = e
}).call(this),
function (e) {
    "use strict";
    var t = document.documentElement,
        n = function () {};
    t.addEventListener ? n = function (e, t, n) {
        e.addEventListener(t, n, !1)
    } : t.attachEvent && (n = function (t, n, i) {
        t[n + i] = i.handleEvent ? function () {
            var t = e.event;
            t.target = t.target || t.srcElement, i.handleEvent.call(i, t)
        } : function () {
            var n = e.event;
            n.target = n.target || n.srcElement, i.call(t, n)
        }, t.attachEvent("on" + n, t[n + i])
    });
    var i = function () {};
    t.removeEventListener ? i = function (e, t, n) {
        e.removeEventListener(t, n, !1)
    } : t.detachEvent && (i = function (e, t, n) {
        e.detachEvent("on" + t, e[t + n]);
        try {
            delete e[t + n]
        } catch (i) {
            e[t + n] = void 0
        }
    });
    var r = {
        bind: n,
        unbind: i
    };
    "function" == typeof define && define.amd ? define(r) : e.eventie = r
}(this),
function (e) {
    "use strict";

    function t(e, t) {
        for (var n in t) e[n] = t[n];
        return e
    }

    function n(e) {
        return "[object Array]" === c.call(e)
    }

    function i(e) {
        var t = [];
        if (n(e)) t = e;
        else if ("number" == typeof e.length)
            for (var i = 0, r = e.length; r > i; i++) t.push(e[i]);
        else t.push(e);
        return t
    }

    function r(e, n) {
        function r(e, n, s) {
            if (!(this instanceof r)) return new r(e, n);
            "string" == typeof e && (e = document.querySelectorAll(e)), this.elements = i(e), this.options = t({}, this.options), "function" == typeof n ? s = n : t(this.options, n), s && this.on("always", s), this.getImages(), o && (this.jqDeferred = new o.Deferred);
            var a = this;
            setTimeout(function () {
                a.check()
            })
        }

        function c(e) {
            this.img = e
        }
        r.prototype = new e, r.prototype.options = {}, r.prototype.getImages = function () {
            this.images = [];
            for (var e = 0, t = this.elements.length; t > e; e++) {
                var n = this.elements[e];
                "IMG" === n.nodeName && this.addImage(n);
                for (var i = n.querySelectorAll("img"), r = 0, o = i.length; o > r; r++) {
                    var s = i[r];
                    this.addImage(s)
                }
            }
        }, r.prototype.addImage = function (e) {
            var t = new c(e);
            this.images.push(t)
        }, r.prototype.check = function () {
            function e(e, r) {
                return t.options.debug && a && s.log("confirm", e, r), t.progress(e), n++, n === i && t.complete(), !0
            }
            var t = this,
                n = 0,
                i = this.images.length;
            if (this.hasAnyBroken = !1, !i) return this.complete(), void 0;
            for (var r = 0; i > r; r++) {
                var o = this.images[r];
                o.on("confirm", e), o.check()
            }
        }, r.prototype.progress = function (e) {
            this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded;
            var t = this;
            setTimeout(function () {
                t.emit("progress", t, e), t.jqDeferred && t.jqDeferred.notify(t, e)
            })
        }, r.prototype.complete = function () {
            var e = this.hasAnyBroken ? "fail" : "done";
            this.isComplete = !0;
            var t = this;
            setTimeout(function () {
                if (t.emit(e, t), t.emit("always", t), t.jqDeferred) {
                    var n = t.hasAnyBroken ? "reject" : "resolve";
                    t.jqDeferred[n](t)
                }
            })
        }, o && (o.fn.imagesLoaded = function (e, t) {
            var n = new r(this, e, t);
            return n.jqDeferred.promise(o(this))
        });
        var f = {};
        return c.prototype = new e, c.prototype.check = function () {
            var e = f[this.img.src];
            if (e) return this.useCached(e), void 0;
            if (f[this.img.src] = this, this.img.complete && void 0 !== this.img.naturalWidth) return this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), void 0;
            var t = this.proxyImage = new Image;
            n.bind(t, "load", this), n.bind(t, "error", this), t.src = this.img.src
        }, c.prototype.useCached = function (e) {
            if (e.isConfirmed) this.confirm(e.isLoaded, "cached was confirmed");
            else {
                var t = this;
                e.on("confirm", function (e) {
                    return t.confirm(e.isLoaded, "cache emitted confirmed"), !0
                })
            }
        }, c.prototype.confirm = function (e, t) {
            this.isConfirmed = !0, this.isLoaded = e, this.emit("confirm", this, t)
        }, c.prototype.handleEvent = function (e) {
            var t = "on" + e.type;
            this[t] && this[t](e)
        }, c.prototype.onload = function () {
            this.confirm(!0, "onload"), this.unbindProxyEvents()
        }, c.prototype.onerror = function () {
            this.confirm(!1, "onerror"), this.unbindProxyEvents()
        }, c.prototype.unbindProxyEvents = function () {
            n.unbind(this.proxyImage, "load", this), n.unbind(this.proxyImage, "error", this)
        }, r
    }
    var o = e.jQuery,
        s = e.console,
        a = s !== void 0,
        c = Object.prototype.toString;
    "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], r) : e.imagesLoaded = r(e.EventEmitter, e.eventie)
}(window);;
var Froogaloop = function () {
    function e(a) {
        return new e.fn.init(a)
    }

    function h(a, c, b) {
        if (!b.contentWindow.postMessage) return !1;
        var f = b.getAttribute("src").split("?")[0],
            a = JSON.stringify({
                method: a,
                value: c
            });
        "//" === f.substr(0, 2) && (f = window.location.protocol + f);
        b.contentWindow.postMessage(a, f)
    }

    function j(a) {
        var c, b;
        try {
            c = JSON.parse(a.data), b = c.event || c.method
        } catch (f) {}
        "ready" == b && !i && (i = !0);
        if (a.origin != k) return !1;
        var a = c.value,
            e = c.data,
            g = "" === g ? null : c.player_id;
        c = g ? d[g][b] : d[b];
        b = [];
        if (!c) return !1;
        void 0 !==
            a && b.push(a);
        e && b.push(e);
        g && b.push(g);
        return 0 < b.length ? c.apply(null, b) : c.call()
    }

    function l(a, c, b) {
        b ? (d[b] || (d[b] = {}), d[b][a] = c) : d[a] = c
    }
    var d = {}, i = !1,
        k = "";
    e.fn = e.prototype = {
        element: null,
        init: function (a) {
            "string" === typeof a && (a = document.getElementById(a));
            this.element = a;
            a = this.element.getAttribute("src");
            "//" === a.substr(0, 2) && (a = window.location.protocol + a);
            for (var a = a.split("/"), c = "", b = 0, f = a.length; b < f; b++) {
                if (3 > b) c += a[b];
                else break;
                2 > b && (c += "/")
            }
            k = c;
            return this
        },
        api: function (a, c) {
            if (!this.element || !a) return !1;
            var b = this.element,
                f = "" !== b.id ? b.id : null,
                d = !c || !c.constructor || !c.call || !c.apply ? c : null,
                e = c && c.constructor && c.call && c.apply ? c : null;
            e && l(a, e, f);
            h(a, d, b);
            return this
        },
        addEvent: function (a, c) {
            if (!this.element) return !1;
            var b = this.element,
                d = "" !== b.id ? b.id : null;
            l(a, c, d);
            "ready" != a ? h("addEventListener", a, b) : "ready" == a && i && c.call(null, d);
            return this
        },
        removeEvent: function (a) {
            if (!this.element) return !1;
            var c = this.element,
                b;
            a: {
                if ((b = "" !== c.id ? c.id : null) && d[b]) {
                    if (!d[b][a]) {
                        b = !1;
                        break a
                    }
                    d[b][a] = null
                } else {
                    if (!d[a]) {
                        b = !1;
                        break a
                    }
                    d[a] = null
                }
                b = !0
            }
            "ready" != a && b && h("removeEventListener", a, c)
        }
    };
    e.fn.init.prototype = e.fn;
    window.addEventListener ? window.addEventListener("message", j, !1) : window.attachEvent("onmessage", j);
    return window.Froogaloop = window.$f = e
}();;
/*!
 * Packery PACKAGED v1.1.1
 * bin-packing layout library
 * http://packery.metafizzy.co
 *
 * Commercial use requires one-time purchase of a commercial license
 * http://packery.metafizzy.co/license.html
 *
 * Non-commercial use is licensed under the GPL v3 License
 *
 * Copyright 2013 Metafizzy
 */

(function (t) {
    "use strict";

    function e(t) {
        return RegExp("(^|\\s+)" + t + "(\\s+|$)")
    }

    function i(t, e) {
        var i = n(t, e) ? r : o;
        i(t, e)
    }
    var n, o, r;
    "classList" in document.documentElement ? (n = function (t, e) {
        return t.classList.contains(e)
    }, o = function (t, e) {
        t.classList.add(e)
    }, r = function (t, e) {
        t.classList.remove(e)
    }) : (n = function (t, i) {
        return e(i).test(t.className)
    }, o = function (t, e) {
        n(t, e) || (t.className = t.className + " " + e)
    }, r = function (t, i) {
        t.className = t.className.replace(e(i), " ")
    });
    var s = {
        hasClass: n,
        addClass: o,
        removeClass: r,
        toggleClass: i,
        has: n,
        add: o,
        remove: r,
        toggle: i
    };
    "function" == typeof define && define.amd ? define(s) : t.classie = s
})(window),
function (t) {
    "use strict";

    function e(t) {
        if (t) {
            if ("string" == typeof n[t]) return t;
            t = t.charAt(0).toUpperCase() + t.slice(1);
            for (var e, o = 0, r = i.length; r > o; o++)
                if (e = i[o] + t, "string" == typeof n[e]) return e
        }
    }
    var i = "Webkit Moz ms Ms O".split(" "),
        n = document.documentElement.style;
    "function" == typeof define && define.amd ? define(function () {
        return e
    }) : t.getStyleProperty = e
}(window),
function (t) {
    "use strict";

    function e(t) {
        var e = parseFloat(t),
            i = -1 === t.indexOf("%") && !isNaN(e);
        return i && e
    }

    function i() {
        for (var t = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0
        }, e = 0, i = s.length; i > e; e++) {
            var n = s[e];
            t[n] = 0
        }
        return t
    }

    function n(t) {
        function n(t) {
            if ("string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
                var n = r(t);
                if ("none" === n.display) return i();
                var h = {};
                h.width = t.offsetWidth, h.height = t.offsetHeight;
                for (var p = h.isBorderBox = !(!a || !n[a] || "border-box" !== n[a]), u = 0, c = s.length; c > u; u++) {
                    var f = s[u],
                        d = n[f],
                        l = parseFloat(d);
                    h[f] = isNaN(l) ? 0 : l
                }
                var y = h.paddingLeft + h.paddingRight,
                    g = h.paddingTop + h.paddingBottom,
                    m = h.marginLeft + h.marginRight,
                    v = h.marginTop + h.marginBottom,
                    _ = h.borderLeftWidth + h.borderRightWidth,
                    b = h.borderTopWidth + h.borderBottomWidth,
                    x = p && o,
                    E = e(n.width);
                E !== !1 && (h.width = E + (x ? 0 : y + _));
                var w = e(n.height);
                return w !== !1 && (h.height = w + (x ? 0 : g + b)), h.innerWidth = h.width - (y + _), h.innerHeight = h.height - (g + b), h.outerWidth = h.width + m, h.outerHeight = h.height + v, h
            }
        }
        var o, a = t("boxSizing");
        return function () {
            if (a) {
                var t = document.createElement("div");
                t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style[a] = "border-box";
                var i = document.body || document.documentElement;
                i.appendChild(t);
                var n = r(t);
                o = 200 === e(n.width), i.removeChild(t)
            }
        }(), n
    }
    var o = document.defaultView,
        r = o && o.getComputedStyle ? function (t) {
            return o.getComputedStyle(t, null)
        } : function (t) {
            return t.currentStyle
        }, s = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
    "function" == typeof define && define.amd ? define(["get-style-property/get-style-property"], n) : t.getSize = n(t.getStyleProperty)
}(window),
function (t) {
    "use strict";
    var e = document.documentElement,
        i = function () {};
    e.addEventListener ? i = function (t, e, i) {
        t.addEventListener(e, i, !1)
    } : e.attachEvent && (i = function (e, i, n) {
        e[i + n] = n.handleEvent ? function () {
            var e = t.event;
            e.target = e.target || e.srcElement, n.handleEvent.call(n, e)
        } : function () {
            var i = t.event;
            i.target = i.target || i.srcElement, n.call(e, i)
        }, e.attachEvent("on" + i, e[i + n])
    });
    var n = function () {};
    e.removeEventListener ? n = function (t, e, i) {
        t.removeEventListener(e, i, !1)
    } : e.detachEvent && (n = function (t, e, i) {
        t.detachEvent("on" + e, t[e + i]);
        try {
            delete t[e + i]
        } catch (n) {
            t[e + i] = void 0
        }
    });
    var o = {
        bind: i,
        unbind: n
    };
    "function" == typeof define && define.amd ? define(o) : t.eventie = o
}(this),
function (t) {
    "use strict";

    function e(t) {
        "function" == typeof t && (e.isReady ? t() : r.push(t))
    }

    function i(t) {
        var i = "readystatechange" === t.type && "complete" !== o.readyState;
        if (!e.isReady && !i) {
            e.isReady = !0;
            for (var n = 0, s = r.length; s > n; n++) {
                var a = r[n];
                a()
            }
        }
    }

    function n(n) {
        return n.bind(o, "DOMContentLoaded", i), n.bind(o, "readystatechange", i), n.bind(t, "load", i), e
    }
    var o = t.document,
        r = [];
    e.isReady = !1, "function" == typeof define && define.amd ? (e.isReady = "function" == typeof requirejs, define(["eventie/eventie"], n)) : t.docReady = n(t.eventie)
}(this),
function () {
    "use strict";

    function t() {}

    function e(t, e) {
        for (var i = t.length; i--;)
            if (t[i].listener === e) return i;
        return -1
    }

    function i(t) {
        return function () {
            return this[t].apply(this, arguments)
        }
    }
    var n = t.prototype;
    n.getListeners = function (t) {
        var e, i, n = this._getEvents();
        if ("object" == typeof t) {
            e = {};
            for (i in n) n.hasOwnProperty(i) && t.test(i) && (e[i] = n[i])
        } else e = n[t] || (n[t] = []);
        return e
    }, n.flattenListeners = function (t) {
        var e, i = [];
        for (e = 0; t.length > e; e += 1) i.push(t[e].listener);
        return i
    }, n.getListenersAsObject = function (t) {
        var e, i = this.getListeners(t);
        return i instanceof Array && (e = {}, e[t] = i), e || i
    }, n.addListener = function (t, i) {
        var n, o = this.getListenersAsObject(t),
            r = "object" == typeof i;
        for (n in o) o.hasOwnProperty(n) && -1 === e(o[n], i) && o[n].push(r ? i : {
            listener: i,
            once: !1
        });
        return this
    }, n.on = i("addListener"), n.addOnceListener = function (t, e) {
        return this.addListener(t, {
            listener: e,
            once: !0
        })
    }, n.once = i("addOnceListener"), n.defineEvent = function (t) {
        return this.getListeners(t), this
    }, n.defineEvents = function (t) {
        for (var e = 0; t.length > e; e += 1) this.defineEvent(t[e]);
        return this
    }, n.removeListener = function (t, i) {
        var n, o, r = this.getListenersAsObject(t);
        for (o in r) r.hasOwnProperty(o) && (n = e(r[o], i), -1 !== n && r[o].splice(n, 1));
        return this
    }, n.off = i("removeListener"), n.addListeners = function (t, e) {
        return this.manipulateListeners(!1, t, e)
    }, n.removeListeners = function (t, e) {
        return this.manipulateListeners(!0, t, e)
    }, n.manipulateListeners = function (t, e, i) {
        var n, o, r = t ? this.removeListener : this.addListener,
            s = t ? this.removeListeners : this.addListeners;
        if ("object" != typeof e || e instanceof RegExp)
            for (n = i.length; n--;) r.call(this, e, i[n]);
        else
            for (n in e) e.hasOwnProperty(n) && (o = e[n]) && ("function" == typeof o ? r.call(this, n, o) : s.call(this, n, o));
        return this
    }, n.removeEvent = function (t) {
        var e, i = typeof t,
            n = this._getEvents();
        if ("string" === i) delete n[t];
        else if ("object" === i)
            for (e in n) n.hasOwnProperty(e) && t.test(e) && delete n[e];
        else delete this._events;
        return this
    }, n.emitEvent = function (t, e) {
        var i, n, o, r, s = this.getListenersAsObject(t);
        for (o in s)
            if (s.hasOwnProperty(o))
                for (n = s[o].length; n--;) i = s[o][n], i.once === !0 && this.removeListener(t, i.listener), r = i.listener.apply(this, e || []), r === this._getOnceReturnValue() && this.removeListener(t, i.listener);
        return this
    }, n.trigger = i("emitEvent"), n.emit = function (t) {
        var e = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(t, e)
    }, n.setOnceReturnValue = function (t) {
        return this._onceReturnValue = t, this
    }, n._getOnceReturnValue = function () {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }, n._getEvents = function () {
        return this._events || (this._events = {})
    }, "function" == typeof define && define.amd ? define(function () {
        return t
    }) : "object" == typeof module && module.exports ? module.exports = t : this.EventEmitter = t
}.call(this),
function (t) {
    "use strict";

    function e() {}

    function i(t) {
        function i(e) {
            e.prototype.option || (e.prototype.option = function (e) {
                t.isPlainObject(e) && (this.options = t.extend(!0, this.options, e))
            })
        }

        function o(e, i) {
            t.fn[e] = function (o) {
                if ("string" == typeof o) {
                    for (var s = n.call(arguments, 1), a = 0, h = this.length; h > a; a++) {
                        var p = this[a],
                            u = t.data(p, e);
                        if (u)
                            if (t.isFunction(u[o]) && "_" !== o.charAt(0)) {
                                var c = u[o].apply(u, s);
                                if (void 0 !== c) return c
                            } else r("no such method '" + o + "' for " + e + " instance");
                            else r("cannot call methods on " + e + " prior to initialization; " + "attempted to call '" + o + "'")
                    }
                    return this
                }
                return this.each(function () {
                    var n = t.data(this, e);
                    n ? (n.option(o), n._init()) : (n = new i(this, o), t.data(this, e, n))
                })
            }
        }
        if (t) {
            var r = "undefined" == typeof console ? e : function (t) {
                    console.error(t)
                };
            t.bridget = function (t, e) {
                i(e), o(t, e)
            }
        }
    }
    var n = Array.prototype.slice;
    "function" == typeof define && define.amd ? define(["jquery"], i) : i(t.jQuery)
}(window),
function (t, e) {
    "use strict";

    function i(t, e) {
        return t[a](e)
    }

    function n(t) {
        if (!t.parentNode) {
            var e = document.createDocumentFragment();
            e.appendChild(t)
        }
    }

    function o(t, e) {
        n(t);
        for (var i = t.parentNode.querySelectorAll(e), o = 0, r = i.length; r > o; o++)
            if (i[o] === t) return !0;
        return !1
    }

    function r(t, e) {
        return n(t), i(t, e)
    }
    var s, a = function () {
            if (e.matchesSelector) return "matchesSelector";
            for (var t = ["webkit", "moz", "ms", "o"], i = 0, n = t.length; n > i; i++) {
                var o = t[i],
                    r = o + "MatchesSelector";
                if (e[r]) return r
            }
        }();
    if (a) {
        var h = document.createElement("div"),
            p = i(h, "div");
        s = p ? i : r
    } else s = o;
    "function" == typeof define && define.amd ? define(function () {
        return s
    }) : window.matchesSelector = s
}(this, Element.prototype),
function (t) {
    "use strict";

    function e(t, e) {
        for (var i in e) t[i] = e[i];
        return t
    }

    function i(t) {
        for (var e in t) return !1;
        return e = null, !0
    }

    function n(t) {
        return t.replace(/([A-Z])/g, function (t) {
            return "-" + t.toLowerCase()
        })
    }

    function o(t, o, r) {
        function a(t, e) {
            t && (this.element = t, this.layout = e, this.position = {
                x: 0,
                y: 0
            }, this._create())
        }
        var h = r("transition"),
            p = r("transform"),
            u = h && p,
            c = !! r("perspective"),
            f = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "otransitionend",
                transition: "transitionend"
            }[h],
            d = ["transform", "transition", "transitionDuration", "transitionProperty"],
            l = function () {
                for (var t = {}, e = 0, i = d.length; i > e; e++) {
                    var n = d[e],
                        o = r(n);
                    o && o !== n && (t[n] = o)
                }
                return t
            }();
        e(a.prototype, t.prototype), a.prototype._create = function () {
            this._transition = {
                ingProperties: {},
                clean: {},
                onEnd: {}
            }, this.css({
                position: "absolute"
            })
        }, a.prototype.handleEvent = function (t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, a.prototype.getSize = function () {
            this.size = o(this.element)
        }, a.prototype.css = function (t) {
            var e = this.element.style;
            for (var i in t) {
                var n = l[i] || i;
                e[n] = t[i]
            }
        }, a.prototype.getPosition = function () {
            var t = s(this.element),
                e = this.layout.options,
                i = e.isOriginLeft,
                n = e.isOriginTop,
                o = parseInt(t[i ? "left" : "right"], 10),
                r = parseInt(t[n ? "top" : "bottom"], 10);
            o = isNaN(o) ? 0 : o, r = isNaN(r) ? 0 : r;
            var a = this.layout.size;
            o -= i ? a.paddingLeft : a.paddingRight, r -= n ? a.paddingTop : a.paddingBottom, this.position.x = o, this.position.y = r
        }, a.prototype.layoutPosition = function () {
            var t = this.layout.size,
                e = this.layout.options,
                i = {};
            e.isOriginLeft ? (i.left = this.position.x + t.paddingLeft + "px", i.right = "") : (i.right = this.position.x + t.paddingRight + "px", i.left = ""), e.isOriginTop ? (i.top = this.position.y + t.paddingTop + "px", i.bottom = "") : (i.bottom = this.position.y + t.paddingBottom + "px", i.top = ""), this.css(i), this.emitEvent("layout", [this])
        };
        var y = c ? function (t, e) {
                return "translate3d(" + t + "px, " + e + "px, 0)"
            } : function (t, e) {
                return "translate(" + t + "px, " + e + "px)"
            };
        a.prototype._transitionTo = function (t, e) {
            this.getPosition();
            var i = this.position.x,
                n = this.position.y,
                o = parseInt(t, 10),
                r = parseInt(e, 10),
                s = o === this.position.x && r === this.position.y;
            if (this.setPosition(t, e), s && !this.isTransitioning) return this.layoutPosition(), void 0;
            var a = t - i,
                h = e - n,
                p = {}, u = this.layout.options;
            a = u.isOriginLeft ? a : -a, h = u.isOriginTop ? h : -h, p.transform = y(a, h), this.transition({
                to: p,
                onTransitionEnd: {
                    transform: this.layoutPosition
                },
                isCleaning: !0
            })
        }, a.prototype.goTo = function (t, e) {
            this.setPosition(t, e), this.layoutPosition()
        }, a.prototype.moveTo = u ? a.prototype._transitionTo : a.prototype.goTo, a.prototype.setPosition = function (t, e) {
            this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
        }, a.prototype._nonTransition = function (t) {
            this.css(t.to), t.isCleaning && this._removeStyles(t.to);
            for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this)
        }, a.prototype._transition = function (t) {
            if (!parseFloat(this.layout.options.transitionDuration)) return this._nonTransition(t), void 0;
            var e = this._transition;
            for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
            for (i in t.to) e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
            if (t.from) {
                this.css(t.from);
                var n = this.element.offsetHeight;
                n = null
            }
            this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
        };
        var g = p && n(p) + ",opacity";
        a.prototype.enableTransition = function () {
            this.isTransitioning || (this.css({
                transitionProperty: g,
                transitionDuration: this.layout.options.transitionDuration
            }), this.element.addEventListener(f, this, !1))
        }, a.prototype.transition = a.prototype[h ? "_transition" : "_nonTransition"], a.prototype.onwebkitTransitionEnd = function (t) {
            this.ontransitionend(t)
        }, a.prototype.onotransitionend = function (t) {
            this.ontransitionend(t)
        };
        var m = {
            "-webkit-transform": "transform",
            "-moz-transform": "transform",
            "-o-transform": "transform"
        };
        a.prototype.ontransitionend = function (t) {
            if (t.target === this.element) {
                var e = this._transition,
                    n = m[t.propertyName] || t.propertyName;
                if (delete e.ingProperties[n], i(e.ingProperties) && this.disableTransition(), n in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[n]), n in e.onEnd) {
                    var o = e.onEnd[n];
                    o.call(this), delete e.onEnd[n]
                }
                this.emitEvent("transitionEnd", [this])
            }
        }, a.prototype.disableTransition = function () {
            this.removeTransitionStyles(), this.element.removeEventListener(f, this, !1), this.isTransitioning = !1
        }, a.prototype._removeStyles = function (t) {
            var e = {};
            for (var i in t) e[i] = "";
            this.css(e)
        };
        var v = {
            transitionProperty: "",
            transitionDuration: ""
        };
        return a.prototype.removeTransitionStyles = function () {
            this.css(v)
        }, a.prototype.removeElem = function () {
            this.element.parentNode.removeChild(this.element), this.emitEvent("remove", [this])
        }, a.prototype.remove = function () {
            if (!h || !parseFloat(this.layout.options.transitionDuration)) return this.removeElem(), void 0;
            var t = this;
            this.on("transitionEnd", function () {
                return t.removeElem(), !0
            }), this.hide()
        }, a.prototype.reveal = function () {
            delete this.isHidden, this.css({
                display: ""
            });
            var t = this.layout.options;
            this.transition({
                from: t.hiddenStyle,
                to: t.visibleStyle,
                isCleaning: !0
            })
        }, a.prototype.hide = function () {
            this.isHidden = !0, this.css({
                display: ""
            });
            var t = this.layout.options;
            this.transition({
                from: t.visibleStyle,
                to: t.hiddenStyle,
                isCleaning: !0,
                onTransitionEnd: {
                    opacity: function () {
                        this.css({
                            display: "none"
                        })
                    }
                }
            })
        }, a.prototype.destroy = function () {
            this.css({
                position: "",
                left: "",
                right: "",
                top: "",
                bottom: "",
                transition: "",
                transform: ""
            })
        }, a
    }
    var r = document.defaultView,
        s = r && r.getComputedStyle ? function (t) {
            return r.getComputedStyle(t, null)
        } : function (t) {
            return t.currentStyle
        };
    "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property"], o) : (t.Outlayer = {}, t.Outlayer.Item = o(t.EventEmitter, t.getSize, t.getStyleProperty))
}(window),
function (t) {
    "use strict";

    function e(t, e) {
        for (var i in e) t[i] = e[i];
        return t
    }

    function i(t) {
        return "[object Array]" === u.call(t)
    }

    function n(t) {
        var e = [];
        if (i(t)) e = t;
        else if (t && "number" == typeof t.length)
            for (var n = 0, o = t.length; o > n; n++) e.push(t[n]);
        else e.push(t);
        return e
    }

    function o(t) {
        return t.replace(/(.)([A-Z])/g, function (t, e, i) {
            return e + "-" + i
        }).toLowerCase()
    }

    function r(i, r, u, d, l, y) {
        function g(t, i) {
            if ("string" == typeof t && (t = s.querySelector(t)), !t || !c(t)) return a && a.error("Bad " + this.settings.namespace + " element: " + t), void 0;
            this.element = t, this.options = e({}, this.options), this.option(i);
            var n = ++v;
            this.element.outlayerGUID = n, _[n] = this, this._create(), this.options.isInitLayout && this.layout()
        }

        function m(t, i) {
            t.prototype[i] = e({}, g.prototype[i])
        }
        var v = 0,
            _ = {};
        return g.prototype.settings = {
            namespace: "outlayer",
            item: y
        }, g.prototype.options = {
            containerStyle: {
                position: "relative"
            },
            isInitLayout: !0,
            isOriginLeft: !0,
            isOriginTop: !0,
            isResizeBound: !0,
            transitionDuration: "0.4s",
            hiddenStyle: {
                opacity: 0,
                transform: "scale(0.001)"
            },
            visibleStyle: {
                opacity: 1,
                transform: "scale(1)"
            }
        }, e(g.prototype, u.prototype), g.prototype.option = function (t) {
            e(this.options, t)
        }, g.prototype._create = function () {
            this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), e(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize()
        }, g.prototype.reloadItems = function () {
            this.items = this._getItems(this.element.children)
        }, g.prototype._getItems = function (t) {
            for (var e = this._filterFindItemElements(t), i = this.settings.item, n = [], o = 0, r = e.length; r > o; o++) {
                var s = e[o],
                    a = new i(s, this, this.options.itemOptions);
                n.push(a)
            }
            return n
        }, g.prototype._filterFindItemElements = function (t) {
            t = n(t);
            for (var e = this.options.itemSelector, i = [], o = 0, r = t.length; r > o; o++) {
                var s = t[o];
                if (c(s))
                    if (e) {
                        l(s, e) && i.push(s);
                        for (var a = s.querySelectorAll(e), h = 0, p = a.length; p > h; h++) i.push(a[h])
                    } else i.push(s)
            }
            return i
        }, g.prototype.getItemElements = function () {
            for (var t = [], e = 0, i = this.items.length; i > e; e++) t.push(this.items[e].element);
            return t
        }, g.prototype.layout = function () {
            this._resetLayout(), this._manageStamps();
            var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
            this.layoutItems(this.items, t), this._isLayoutInited = !0
        }, g.prototype._init = g.prototype.layout, g.prototype._resetLayout = function () {
            this.getSize()
        }, g.prototype.getSize = function () {
            this.size = d(this.element)
        }, g.prototype._getMeasurement = function (t, e) {
            var i, n = this.options[t];
            n ? ("string" == typeof n ? i = this.element.querySelector(n) : c(n) && (i = n), this[t] = i ? d(i)[e] : n) : this[t] = 0
        }, g.prototype.layoutItems = function (t, e) {
            t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
        }, g.prototype._getItemsForLayout = function (t) {
            for (var e = [], i = 0, n = t.length; n > i; i++) {
                var o = t[i];
                o.isIgnored || e.push(o)
            }
            return e
        }, g.prototype._layoutItems = function (t, e) {
            if (!t || !t.length) return this.emitEvent("layoutComplete", [this, t]), void 0;
            this._itemsOn(t, "layout", function () {
                this.emitEvent("layoutComplete", [this, t])
            });
            for (var i = [], n = 0, o = t.length; o > n; n++) {
                var r = t[n],
                    s = this._getItemLayoutPosition(r);
                s.item = r, s.isInstant = e, i.push(s)
            }
            this._processLayoutQueue(i)
        }, g.prototype._getItemLayoutPosition = function () {
            return {
                x: 0,
                y: 0
            }
        }, g.prototype._processLayoutQueue = function (t) {
            for (var e = 0, i = t.length; i > e; e++) {
                var n = t[e];
                this._positionItem(n.item, n.x, n.y, n.isInstant)
            }
        }, g.prototype._positionItem = function (t, e, i, n) {
            n ? t.goTo(e, i) : t.moveTo(e, i)
        }, g.prototype._postLayout = function () {
            var t = this._getContainerSize();
            t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
        }, g.prototype._getContainerSize = p, g.prototype._setContainerMeasure = function (t, e) {
            if (void 0 !== t) {
                var i = this.size;
                i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
            }
        }, g.prototype._itemsOn = function (t, e, i) {
            function n() {
                return o++, o === r && i.call(s), !0
            }
            for (var o = 0, r = t.length, s = this, a = 0, h = t.length; h > a; a++) {
                var p = t[a];
                p.on(e, n)
            }
        }, g.prototype.ignore = function (t) {
            var e = this.getItem(t);
            e && (e.isIgnored = !0)
        }, g.prototype.unignore = function (t) {
            var e = this.getItem(t);
            e && delete e.isIgnored
        }, g.prototype.stamp = function (t) {
            if (t = this._find(t)) {
                this.stamps = this.stamps.concat(t);
                for (var e = 0, i = t.length; i > e; e++) {
                    var n = t[e];
                    this.ignore(n)
                }
            }
        }, g.prototype.unstamp = function (t) {
            if (t = this._find(t))
                for (var e = 0, i = t.length; i > e; e++) {
                    var n = t[e],
                        o = f(this.stamps, n); - 1 !== o && this.stamps.splice(o, 1), this.unignore(n)
                }
        }, g.prototype._find = function (t) {
            return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), t = n(t)) : void 0
        }, g.prototype._manageStamps = function () {
            if (this.stamps && this.stamps.length) {
                this._getBoundingRect();
                for (var t = 0, e = this.stamps.length; e > t; t++) {
                    var i = this.stamps[t];
                    this._manageStamp(i)
                }
            }
        }, g.prototype._getBoundingRect = function () {
            var t = this.element.getBoundingClientRect(),
                e = this.size;
            this._boundingRect = {
                left: t.left + e.paddingLeft + e.borderLeftWidth,
                top: t.top + e.paddingTop + e.borderTopWidth,
                right: t.right - (e.paddingRight + e.borderRightWidth),
                bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
            }
        }, g.prototype._manageStamp = p, g.prototype._getElementOffset = function (t) {
            var e = t.getBoundingClientRect(),
                i = this._boundingRect,
                n = d(t),
                o = {
                    left: e.left - i.left - n.marginLeft,
                    top: e.top - i.top - n.marginTop,
                    right: i.right - e.right - n.marginRight,
                    bottom: i.bottom - e.bottom - n.marginBottom
                };
            return o
        }, g.prototype.handleEvent = function (t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, g.prototype.bindResize = function () {
            this.isResizeBound || (i.bind(t, "resize", this), this.isResizeBound = !0)
        }, g.prototype.unbindResize = function () {
            i.unbind(t, "resize", this), this.isResizeBound = !1
        }, g.prototype.onresize = function () {
            function t() {
                e.resize(), delete e.resizeTimeout
            }
            this.resizeTimeout && clearTimeout(this.resizeTimeout);
            var e = this;
            this.resizeTimeout = setTimeout(t, 100)
        }, g.prototype.resize = function () {
            var t = d(this.element),
                e = this.size && t;
            e && t.innerWidth === this.size.innerWidth || this.layout()
        }, g.prototype.addItems = function (t) {
            var e = this._getItems(t);
            if (e.length) return this.items = this.items.concat(e), e
        }, g.prototype.appended = function (t) {
            var e = this.addItems(t);
            e.length && (this.layoutItems(e, !0), this.reveal(e))
        }, g.prototype.prepended = function (t) {
            var e = this._getItems(t);
            if (e.length) {
                var i = this.items.slice(0);
                this.items = e.concat(i), this._resetLayout(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i)
            }
        }, g.prototype.reveal = function (t) {
            if (t && t.length)
                for (var e = 0, i = t.length; i > e; e++) {
                    var n = t[e];
                    n.reveal()
                }
        }, g.prototype.hide = function (t) {
            if (t && t.length)
                for (var e = 0, i = t.length; i > e; e++) {
                    var n = t[e];
                    n.hide()
                }
        }, g.prototype.getItem = function (t) {
            for (var e = 0, i = this.items.length; i > e; e++) {
                var n = this.items[e];
                if (n.element === t) return n
            }
        }, g.prototype.getItems = function (t) {
            if (t && t.length) {
                for (var e = [], i = 0, n = t.length; n > i; i++) {
                    var o = t[i],
                        r = this.getItem(o);
                    r && e.push(r)
                }
                return e
            }
        }, g.prototype.remove = function (t) {
            t = n(t);
            var e = this.getItems(t);
            if (e && e.length) {
                this._itemsOn(e, "remove", function () {
                    this.emitEvent("removeComplete", [this, e])
                });
                for (var i = 0, o = e.length; o > i; i++) {
                    var r = e[i];
                    r.remove();
                    var s = f(this.items, r);
                    this.items.splice(s, 1)
                }
            }
        }, g.prototype.destroy = function () {
            var t = this.element.style;
            t.height = "", t.position = "", t.width = "";
            for (var e = 0, i = this.items.length; i > e; e++) {
                var n = this.items[e];
                n.destroy()
            }
            this.unbindResize(), delete this.element.outlayerGUID, h && h.removeData(this.element, this.settings.namespace)
        }, g.data = function (t) {
            var e = t && t.outlayerGUID;
            return e && _[e]
        }, g.create = function (t, i) {
            function n() {
                g.apply(this, arguments)
            }
            return e(n.prototype, g.prototype), m(n, "options"), m(n, "settings"), e(n.prototype.options, i), n.prototype.settings.namespace = t, n.data = g.data, n.Item = function () {
                y.apply(this, arguments)
            }, n.Item.prototype = new y, n.prototype.settings.item = n.Item, r(function () {
                for (var e = o(t), i = s.querySelectorAll(".js-" + e), r = "data-" + e + "-options", p = 0, u = i.length; u > p; p++) {
                    var c, f = i[p],
                        d = f.getAttribute(r);
                    try {
                        c = d && JSON.parse(d)
                    } catch (l) {
                        a && a.error("Error parsing " + r + " on " + f.nodeName.toLowerCase() + (f.id ? "#" + f.id : "") + ": " + l);
                        continue
                    }
                    var y = new n(f, c);
                    h && h.data(f, t, y)
                }
            }), h && h.bridget && h.bridget(t, n), n
        }, g.Item = y, g
    }
    var s = t.document,
        a = t.console,
        h = t.jQuery,
        p = function () {}, u = Object.prototype.toString,
        c = "object" == typeof HTMLElement ? function (t) {
            return t instanceof HTMLElement
        } : function (t) {
            return t && "object" == typeof t && 1 === t.nodeType && "string" == typeof t.nodeName
        }, f = Array.prototype.indexOf ? function (t, e) {
            return t.indexOf(e)
        } : function (t, e) {
            for (var i = 0, n = t.length; n > i; i++)
                if (t[i] === e) return i;
            return -1
        };
    "function" == typeof define && define.amd ? define(["eventie/eventie", "doc-ready/doc-ready", "eventEmitter/EventEmitter", "get-size/get-size", "matches-selector/matches-selector", "./item"], r) : t.Outlayer = r(t.eventie, t.docReady, t.EventEmitter, t.getSize, t.matchesSelector, t.Outlayer.Item)
}(window),
function (t) {
    "use strict";

    function e() {
        function t(e) {
            for (var i in t.defaults) this[i] = t.defaults[i];
            for (i in e) this[i] = e[i]
        }
        return i.Rect = t, t.defaults = {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        }, t.prototype.contains = function (t) {
            var e = t.width || 0,
                i = t.height || 0;
            return this.x <= t.x && this.y <= t.y && this.x + this.width >= t.x + e && this.y + this.height >= t.y + i
        }, t.prototype.overlaps = function (t) {
            var e = this.x + this.width,
                i = this.y + this.height,
                n = t.x + t.width,
                o = t.y + t.height;
            return n > this.x && e > t.x && o > this.y && i > t.y
        }, t.prototype.getMaximalFreeRects = function (e) {
            if (!this.overlaps(e)) return !1;
            var i, n = [],
                o = this.x + this.width,
                r = this.y + this.height,
                s = e.x + e.width,
                a = e.y + e.height;
            return this.y < e.y && (i = new t({
                x: this.x,
                y: this.y,
                width: this.width,
                height: e.y - this.y
            }), n.push(i)), o > s && (i = new t({
                x: s,
                y: this.y,
                width: o - s,
                height: this.height
            }), n.push(i)), r > a && (i = new t({
                x: this.x,
                y: a,
                width: this.width,
                height: r - a
            }), n.push(i)), this.x < e.x && (i = new t({
                x: this.x,
                y: this.y,
                width: e.x - this.x,
                height: this.height
            }), n.push(i)), n
        }, t.prototype.canFit = function (t) {
            return this.width >= t.width && this.height >= t.height
        }, t
    }
    var i = t.Packery = function () {};
    "function" == typeof define && define.amd ? define(e) : (t.Packery = t.Packery || {}, t.Packery.Rect = e())
}(window),
function (t) {
    "use strict";

    function e(t) {
        function e(t, e) {
            this.width = t || 0, this.height = e || 0, this.reset()
        }
        return e.prototype.reset = function () {
            this.spaces = [], this.newSpaces = [];
            var e = new t({
                x: 0,
                y: 0,
                width: this.width,
                height: this.height
            });
            this.spaces.push(e)
        }, e.prototype.pack = function (t) {
            for (var e = 0, i = this.spaces.length; i > e; e++) {
                var n = this.spaces[e];
                if (n.canFit(t)) {
                    this.placeInSpace(t, n);
                    break
                }
            }
        }, e.prototype.placeInSpace = function (t, e) {
            t.x = e.x, t.y = e.y, this.placed(t)
        }, e.prototype.placed = function (t) {
            for (var i = [], n = 0, o = this.spaces.length; o > n; n++) {
                var r = this.spaces[n],
                    s = r.getMaximalFreeRects(t);
                s ? i.push.apply(i, s) : i.push(r)
            }
            this.spaces = i, e.mergeRects(this.spaces), this.spaces.sort(e.spaceSorterTopLeft)
        }, e.mergeRects = function (t) {
            for (var e = 0, i = t.length; i > e; e++) {
                var n = t[e];
                if (n) {
                    var o = t.slice(0);
                    o.splice(e, 1);
                    for (var r = 0, s = 0, a = o.length; a > s; s++) {
                        var h = o[s],
                            p = e > s ? 0 : 1;
                        n.contains(h) && (t.splice(s + p - r, 1), r++)
                    }
                }
            }
            return t
        }, e.spaceSorterTopLeft = function (t, e) {
            return t.y - e.y || t.x - e.x
        }, e.spaceSorterLeftTop = function (t, e) {
            return t.x - e.x || t.y - e.y
        }, e
    }
    if ("function" == typeof define && define.amd) define(["./rect"], e);
    else {
        var i = t.Packery = t.Packery || {};
        i.Packer = e(i.Rect)
    }
}(window),
function (t) {
    "use strict";

    function e(t, e, i) {
        var n = t("transform"),
            o = function () {
                e.Item.apply(this, arguments)
            };
        o.prototype = new e.Item;
        var r = o.prototype._create;
        return o.prototype._create = function () {
            r.call(this), this.rect = new i, this.placeRect = new i
        }, o.prototype.dragStart = function () {
            this.getPosition(), this.removeTransitionStyles(), this.isTransitioning && n && (this.element.style[n] = "none"), this.getSize(), this.isPlacing = !0, this.needsPositioning = !1, this.positionPlaceRect(this.position.x, this.position.y), this.isTransitioning = !1, this.didDrag = !1
        }, o.prototype.dragMove = function (t, e) {
            this.didDrag = !0;
            var i = this.layout.size;
            t -= i.paddingLeft, e -= i.paddingTop, this.positionPlaceRect(t, e)
        }, o.prototype.dragStop = function () {
            this.getPosition();
            var t = this.position.x !== this.placeRect.x,
                e = this.position.y !== this.placeRect.y;
            this.needsPositioning = t || e, this.didDrag = !1
        }, o.prototype.positionPlaceRect = function (t, e, i) {
            this.placeRect.x = this.getPlaceRectCoord(t, !0), this.placeRect.y = this.getPlaceRectCoord(e, !1, i)
        }, o.prototype.getPlaceRectCoord = function (t, e, i) {
            var n = e ? "Width" : "Height",
                o = this.size["outer" + n],
                r = this.layout[e ? "columnWidth" : "rowHeight"],
                s = this.layout.size["inner" + n];
            e || (s = Math.max(s, this.layout.maxY), this.layout.rowHeight || (s -= this.layout.gutter));
            var a;
            if (r) {
                r += this.layout.gutter, s += e ? this.layout.gutter : 0, t = Math.round(t / r);
                var h = Math[e ? "floor" : "ceil"](s / r);
                h -= Math.ceil(o / r), a = h
            } else a = s - o;
            return t = i ? t : Math.min(t, a), t *= r || 1, Math.max(0, t)
        }, o.prototype.copyPlaceRectPosition = function () {
            this.rect.x = this.placeRect.x, this.rect.y = this.placeRect.y
        }, o
    }
    "function" == typeof define && define.amd ? define(["get-style-property/get-style-property", "outlayer/outlayer", "./rect"], e) : t.Packery.Item = e(t.getStyleProperty, t.Outlayer, t.Packery.Rect)
}(window),
function (t) {
    "use strict";

    function e(t, e, i, n, o, r) {
        var s = i.create("packery");
        return s.Item = s.prototype.settings.item = r, s.prototype._create = function () {
            i.prototype._create.call(this), this.packer = new o, this.stamp(this.options.stamped);
            var t = this;
            this.handleDraggabilly = {
                dragStart: function (e) {
                    t.itemDragStart(e.element)
                },
                dragMove: function (e) {
                    t.itemDragMove(e.element, e.position.x, e.position.y)
                },
                dragEnd: function (e) {
                    t.itemDragEnd(e.element)
                }
            }, this.handleUIDraggable = {
                start: function (e) {
                    t.itemDragStart(e.currentTarget)
                },
                drag: function (e, i) {
                    t.itemDragMove(e.currentTarget, i.position.left, i.position.top)
                },
                stop: function (e) {
                    t.itemDragEnd(e.currentTarget)
                }
            }
        }, s.prototype._resetLayout = function () {
            this.getSize(), this._getMeasurements(), this.packer.width = this.size.innerWidth + this.gutter, this.packer.height = Number.POSITIVE_INFINITY, this.packer.reset(), this.maxY = 0
        }, s.prototype._getMeasurements = function () {
            this._getMeasurement("columnWidth", "width"), this._getMeasurement("rowHeight", "height"), this._getMeasurement("gutter", "width")
        }, s.prototype._getItemLayoutPosition = function (t) {
            return this._packItem(t), t.rect
        }, s.prototype._packItem = function (t) {
            this._setRectSize(t.element, t.rect), this.packer.pack(t.rect), this._setMaxY(t.rect)
        }, s.prototype._setMaxY = function (t) {
            this.maxY = Math.max(t.y + t.height, this.maxY)
        }, s.prototype._setRectSize = function (t, i) {
            var n = e(t),
                o = n.outerWidth,
                r = n.outerHeight,
                s = this.columnWidth + this.gutter,
                a = this.rowHeight + this.gutter;
            o = this.columnWidth ? Math.ceil(o / s) * s : o + this.gutter, r = this.rowHeight ? Math.ceil(r / a) * a : r + this.gutter, i.width = Math.min(o, this.packer.width), i.height = r
        }, s.prototype._getContainerSize = function () {
            return {
                height: this.maxY - this.gutter
            }
        }, s.prototype._manageStamp = function (t) {
            var e, i = this.getItem(t);
            if (i && i.isPlacing) e = i.placeRect;
            else {
                var o = this._getElementOffset(t);
                e = new n({
                    x: o.left,
                    y: o.top
                })
            }
            this._setRectSize(t, e), this.packer.placed(e), this._setMaxY(e)
        }, s.prototype.sortItemsByPosition = function () {
            this.items.sort(function (t, e) {
                return t.position.y - e.position.y || t.position.x - e.position.x
            })
        }, s.prototype.fit = function (t, e, i) {
            var n = this.getItem(t);
            n && (this._getMeasurements(), this.stamp(n.element), n.getSize(), n.isPlacing = !0, e = void 0 === e ? n.rect.x : e, i = void 0 === i ? n.rect.y : i, n.positionPlaceRect(e, i, !0), this._bindFitEvents(n), n.moveTo(n.placeRect.x, n.placeRect.y), this.layout(), this.unstamp(n.element), this.sortItemsByPosition(), n.isPlacing = !1, n.copyPlaceRectPosition())
        }, s.prototype._bindFitEvents = function (t) {
            function e() {
                n++, 2 === n && i.emitEvent("fitComplete", [i, t])
            }
            var i = this,
                n = 0;
            t.on("layout", function () {
                return e(), !0
            }), this.on("layoutComplete", function () {
                return e(), !0
            })
        }, s.prototype.itemDragStart = function (t) {
            this.stamp(t);
            var e = this.getItem(t);
            e && e.dragStart()
        }, s.prototype.itemDragMove = function (t, e, i) {
            function n() {
                r.layout(), delete r.dragTimeout
            }
            var o = this.getItem(t);
            o && o.dragMove(e, i);
            var r = this;
            this.clearDragTimeout(), this.dragTimeout = setTimeout(n, 40)
        }, s.prototype.clearDragTimeout = function () {
            this.dragTimeout && clearTimeout(this.dragTimeout)
        }, s.prototype.itemDragEnd = function (e) {
            var i, n = this.getItem(e);
            if (n && (i = n.didDrag, n.dragStop()), !n || !i && !n.needsPositioning) return this.unstamp(e), void 0;
            t.add(n.element, "is-positioning-post-drag");
            var o = this._getDragEndLayoutComplete(e, n);
            n.needsPositioning ? (n.on("layout", o), n.moveTo(n.placeRect.x, n.placeRect.y)) : n && n.copyPlaceRectPosition(), this.clearDragTimeout(), this.on("layoutComplete", o), this.layout()
        }, s.prototype._getDragEndLayoutComplete = function (e, i) {
            var n = i && i.needsPositioning,
                o = 0,
                r = n ? 2 : 1,
                s = this;
            return function () {
                return o++, o !== r ? !0 : (i && (t.remove(i.element, "is-positioning-post-drag"), i.isPlacing = !1, i.copyPlaceRectPosition()), s.unstamp(e), s.sortItemsByPosition(), n && s.emitEvent("dragItemPositioned", [s, i]), !0)
            }
        }, s.prototype.bindDraggabillyEvents = function (t) {
            t.on("dragStart", this.handleDraggabilly.dragStart), t.on("dragMove", this.handleDraggabilly.dragMove), t.on("dragEnd", this.handleDraggabilly.dragEnd)
        }, s.prototype.bindUIDraggableEvents = function (t) {
            t.on("dragstart", this.handleUIDraggable.start).on("drag", this.handleUIDraggable.drag).on("dragstop", this.handleUIDraggable.stop)
        }, s.Rect = n, s.Packer = o, s
    }
    "function" == typeof define && define.amd ? define(["classie/classie", "get-size/get-size", "outlayer/outlayer", "./rect", "./packer", "./item"], e) : t.Packery = e(t.classie, t.getSize, t.Outlayer, t.Packery.Rect, t.Packery.Packer, t.Packery.Item)
}(window);;

function Swipe(container, options) {
    "use strict";
    var noop = function () {};
    var offloadFn = function (fn) {
        setTimeout(fn || noop, 0)
    };
    var browser = {
        addEventListener: !! window.addEventListener,
        touch: ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
        transitions: (function (temp) {
            var props = ['transitionProperty', 'WebkitTransition', 'MozTransition', 'OTransition', 'msTransition'];
            for (var i in props)
                if (temp.style[props[i]] !== undefined) return true;
            return false;
        })(document.createElement('swipe'))
    };
    if (!container) return;
    var element = container.children[0];
    var slides, slidePos, width, length;
    options = options || {};
    var index = parseInt(options.startSlide, 10) || 0;
    var speed = options.speed || 300;
    options.continuous = options.continuous !== undefined ? options.continuous : true;

    function setup() {
        slides = element.children;
        length = slides.length;
        if (slides.length < 2) options.continuous = false;
        if (browser.transitions && options.continuous && slides.length < 3) {
            element.appendChild(slides[0].cloneNode(true));
            element.appendChild(element.children[1].cloneNode(true));
            slides = element.children;
        }
        slidePos = new Array(slides.length);
        width = container.getBoundingClientRect().width || container.offsetWidth;
        element.style.width = (slides.length * width) + 'px';
        var pos = slides.length;
        while (pos--) {
            var slide = slides[pos];
            slide.style.width = width + 'px';
            slide.setAttribute('data-index', pos);
            if (browser.transitions) {
                slide.style.left = (pos * -width) + 'px';
                move(pos, index > pos ? -width : (index < pos ? width : 0), 0);
            }
        }
        if (options.continuous && browser.transitions) {
            move(circle(index - 1), -width, 0);
            move(circle(index + 1), width, 0);
        }
        if (!browser.transitions) element.style.left = (index * -width) + 'px';
        container.style.visibility = 'visible';
    }

    function prev() {
        if (options.continuous) slide(index - 1);
        else if (index) slide(index - 1);
    }

    function next() {
        if (options.continuous) slide(index + 1);
        else if (index < slides.length - 1) slide(index + 1);
    }

    function circle(index) {
        return (slides.length + (index % slides.length)) % slides.length;
    }

    function slide(to, slideSpeed) {
        if (index == to) return;
        if (browser.transitions) {
            var direction = Math.abs(index - to) / (index - to);
            if (options.continuous) {
                var natural_direction = direction;
                direction = -slidePos[circle(to)] / width;
                if (direction !== natural_direction) to = -direction * slides.length + to;
            }
            var diff = Math.abs(index - to) - 1;
            while (diff--) move(circle((to > index ? to : index) - diff - 1), width * direction, 0);
            to = circle(to);
            move(index, width * direction, slideSpeed || speed);
            move(to, 0, slideSpeed || speed);
            if (options.continuous) move(circle(to - direction), -(width * direction), 0);
        } else {
            to = circle(to);
            animate(index * -width, to * -width, slideSpeed || speed);
        }
        index = to;
        offloadFn(options.callback && options.callback(index, slides[index]));
    }

    function move(index, dist, speed) {
        translate(index, dist, speed);
        slidePos[index] = dist;
    }

    function translate(index, dist, speed) {
        var slide = slides[index];
        var style = slide && slide.style;
        if (!style) return;
        style.webkitTransitionDuration = style.MozTransitionDuration = style.msTransitionDuration = style.OTransitionDuration = style.transitionDuration = speed + 'ms';
        style.webkitTransform = 'translate(' + dist + 'px,0)' + 'translateZ(0)';
        style.msTransform = style.MozTransform = style.OTransform = 'translateX(' + dist + 'px)';
    }

    function animate(from, to, speed) {
        if (!speed) {
            element.style.left = to + 'px';
            return;
        }
        var start = +new Date;
        var timer = setInterval(function () {
            var timeElap = +new Date - start;
            if (timeElap > speed) {
                element.style.left = to + 'px';
                if (delay) begin();
                options.transitionEnd && options.transitionEnd.call(event, index, slides[index]);
                clearInterval(timer);
                return;
            }
            element.style.left = (((to - from) * (Math.floor((timeElap / speed) * 100) / 100)) + from) + 'px';
        }, 4);
    }
    var delay = options.auto || 0;
    var interval;

    function begin() {
        interval = setTimeout(next, delay);
    }

    function stop() {
        delay = 0;
        clearTimeout(interval);
    }
    var start = {};
    var delta = {};
    var isScrolling;
    var events = {
        handleEvent: function (event) {
            switch (event.type) {
            case 'touchstart':
                this.start(event);
                break;
            case 'touchmove':
                this.move(event);
                break;
            case 'touchend':
                offloadFn(this.end(event));
                break;
            case 'webkitTransitionEnd':
            case 'msTransitionEnd':
            case 'oTransitionEnd':
            case 'otransitionend':
            case 'transitionend':
                offloadFn(this.transitionEnd(event));
                break;
            case 'resize':
                offloadFn(setup.call());
                break;
            }
            if (options.stopPropagation) event.stopPropagation();
        },
        start: function (event) {
            var touches = event.touches[0];
            start = {
                x: touches.pageX,
                y: touches.pageY,
                time: +new Date
            };
            isScrolling = undefined;
            delta = {};
            element.addEventListener('touchmove', this, false);
            element.addEventListener('touchend', this, false);
        },
        move: function (event) {
            if (event.touches.length > 1 || event.scale && event.scale !== 1) return
            if (options.disableScroll) event.preventDefault();
            var touches = event.touches[0];
            delta = {
                x: touches.pageX - start.x,
                y: touches.pageY - start.y
            }
            if (typeof isScrolling == 'undefined') {
                isScrolling = !! (isScrolling || Math.abs(delta.x) < Math.abs(delta.y));
            }
            if (!isScrolling) {
                event.preventDefault();
                stop();
                if (options.continuous) {
                    translate(circle(index - 1), delta.x + slidePos[circle(index - 1)], 0);
                    translate(index, delta.x + slidePos[index], 0);
                    translate(circle(index + 1), delta.x + slidePos[circle(index + 1)], 0);
                } else {
                    delta.x = delta.x / ((!index && delta.x > 0 || index == slides.length - 1 && delta.x < 0) ? (Math.abs(delta.x) / width + 1) : 1);
                    translate(index - 1, delta.x + slidePos[index - 1], 0);
                    translate(index, delta.x + slidePos[index], 0);
                    translate(index + 1, delta.x + slidePos[index + 1], 0);
                }
            }
        },
        end: function (event) {
            var duration = +new Date - start.time;
            var isValidSlide = Number(duration) < 250 && Math.abs(delta.x) > 20 || Math.abs(delta.x) > width / 2;
            var isPastBounds = !index && delta.x > 0 || index == slides.length - 1 && delta.x < 0;
            if (options.continuous) isPastBounds = false;
            var direction = delta.x < 0;
            if (!isScrolling) {
                if (isValidSlide && !isPastBounds) {
                    if (direction) {
                        if (options.continuous) {
                            move(circle(index - 1), -width, 0);
                            move(circle(index + 2), width, 0);
                        } else {
                            move(index - 1, -width, 0);
                        }
                        move(index, slidePos[index] - width, speed);
                        move(circle(index + 1), slidePos[circle(index + 1)] - width, speed);
                        index = circle(index + 1);
                    } else {
                        if (options.continuous) {
                            move(circle(index + 1), width, 0);
                            move(circle(index - 2), -width, 0);
                        } else {
                            move(index + 1, width, 0);
                        }
                        move(index, slidePos[index] + width, speed);
                        move(circle(index - 1), slidePos[circle(index - 1)] + width, speed);
                        index = circle(index - 1);
                    }
                    options.callback && options.callback(index, slides[index]);
                } else {

                    if (options.continuous) {
                        move(circle(index - 1), -width, speed);
                        move(index, 0, speed);
                        move(circle(index + 1), width, speed);
                    } else {
                        move(index - 1, -width, speed);
                        move(index, 0, speed);
                        move(index + 1, width, speed);
                    }
                }
            }
            element.removeEventListener('touchmove', events, false)
            element.removeEventListener('touchend', events, false)
        },
        transitionEnd: function (event) {
            if (parseInt(event.target.getAttribute('data-index'), 10) == index) {
                if (delay) begin();
                options.transitionEnd && options.transitionEnd.call(event, index, slides[index]);
            }
        }
    }
    setup();
    if (delay) begin();
    if (browser.addEventListener) {
        if (browser.touch) element.addEventListener('touchstart', events, false);
        if (browser.transitions) {
            element.addEventListener('webkitTransitionEnd', events, false);
            element.addEventListener('msTransitionEnd', events, false);
            element.addEventListener('oTransitionEnd', events, false);
            element.addEventListener('otransitionend', events, false);
            element.addEventListener('transitionend', events, false);
        }
        window.addEventListener('resize', events, false);
    } else {
        window.onresize = function () {
            setup()
        };
    }
    return {
        setup: function () {
            setup();
        },
        slide: function (to, speed) {
            stop();
            slide(to, speed);
        },
        prev: function () {
            stop();
            prev();
        },
        next: function () {
            stop();
            next();
        },
        getPos: function () {
            return index;
        },
        getNumSlides: function () {
            return length;
        },
        kill: function () {
            stop();
            element.style.width = 'auto';
            element.style.left = 0;
            var pos = slides.length;
            while (pos--) {
                var slide = slides[pos];
                slide.style.width = '100%';
                slide.style.left = 0;
                if (browser.transitions) translate(pos, 0, 0);
            }
            if (browser.addEventListener) {
                element.removeEventListener('touchstart', events, false);
                element.removeEventListener('webkitTransitionEnd', events, false);
                element.removeEventListener('msTransitionEnd', events, false);
                element.removeEventListener('oTransitionEnd', events, false);
                element.removeEventListener('otransitionend', events, false);
                element.removeEventListener('transitionend', events, false);
                window.removeEventListener('resize', events, false);
            } else {
                window.onresize = null;
            }
        }
    }
}
if (window.jQuery || window.Zepto) {
    (function ($) {
        $.fn.Swipe = function (params) {
            return this.each(function () {
                $(this).data('Swipe', new Swipe($(this)[0], params));
            });
        }
    })(window.jQuery || window.Zepto)
};
jQuery.noConflict();
var GSP = function ($) {
    var $window, $body, Site = {
            settings: {
                path: {
                    idPosition: 1
                }
            },
            callAll: function (f, data) {
                var o, que = [];
                for (o in Site) {
                    if (typeof (Site[o]) === "object" && Site[o] !== null) {
                        if (f in Site[o]) {
                            if ("que" in Site[o]) {
                                que.push(Site[o]);
                            } else {
                                Site[o][f](data);
                            }
                        }
                    }
                }
                for (qued in que) {
                    que[qued][f](data);
                }
            },
            onLoad: function () {
                Site.PushStates.overwriteLinks();
                Site.callAll("onLoad");
                Site.setSelected();
                Site.applyStyling();
                Site.scrollToArticle(false);
            },
            onUnload: function () {
                Site.callAll("onUnload");
            },
            onResize: function (instant) {
                Site.callAll("onResize");
            },
            setSelected: function () {
                var path = window.location.pathname;
                if (path == "/") {
                    $("a[href='" + path + "']").parent().addClass("selected");
                    return;
                }
                $("a[href$='" + path + "']").parent().addClass("selected");
            },
            scrollToArticle: function (animate) {
                if (typeof (animate) === "undefined") {
                    animate = 400;
                } else {
                    animate = 0;
                }
                var targetScroll = 0,
                    $article = $("[data-path='" + window.location.pathname + "']");
                Site.PushStates.noChange = true;
                if ($("[data-path='" + window.location.pathname + "']").length) {
                    targetScroll = ($article.offset().top) - 50;
                }
                $("html,body").animate({
                    scrollTop: targetScroll + "px"
                }, animate, function () {
                    Site.PushStates.noChange = false;
                });
            },
            browser: "",
            browserDetect: function () {
                var ua = navigator.userAgent.toLowerCase();
                if (ua.indexOf('safari') != -1 && ua.indexOf("chrome") == -1) Site.browser = "safari";
                if ($(".lt-ie9").length) Site.browser = "oldIE";
                if (Function('/*@cc_on return document.documentMode===10@*/')()) {
                    $("html").addClass("ie");
                    Site.browser = "IE";
                }
            },
            init: function () {
                Site.browserDetect();
                $window.on("resize", Site.onResize);
                Site.onResize(true);
                Site.callAll("init");
                Site.callAll("onLoad");
                Site.PushStates.overwriteLinks();
                Site.applyStyling();
                Site.setSelected();
            },
            applyStyling: function () {}
        };
    Site.Menu = {
        init: function () {
            $("#menu-trigger").off().on("click", Site.Menu.toggleMenu);
            $("#menu-closer").off().on("click", Site.Menu.toggleMenu);
        },
        toggleMenu: function (e) {
            e.preventDefault();
            Site.Menu.clearSearch();
            $body.toggleClass("menu-open");
        },
        searchTime: null,
        clearSearch: function () {
            clearTimeout(Site.Menu.clearSearch);
            Site.Menu.searchTime = setTimeout(function () {
                $("#q").val("");
            }, 400);
        }
    };
    Site.Search = {
        liveSearchUrl: "/api/en/search/",
        init: function () {
            $("#menu").find("form").off().on("submit", Site.Search.onSubmit);
        },
        onSubmit: function (e) {
            e.preventDefault();
            var form = $(e.currentTarget),
                q = form.find("input").val();
            q = encodeURIComponent(q);
            if (q != "") Site.PushStates.goTo("/search/" + q);
        },
        showIt: function (e) {
            e.preventDefault();
            $("#search").toggleClass("selected");
        },
        hideIt: function (e) {
            e.preventDefault();
            $("#search").removeClass("selected");
        },
        timeOut: null,
        onType: function (e) {
            clearTimeout(Site.Search.timeOut);
            if ($(e.currentTarget).val().length > 1) {
                Site.Search.timeOut = setTimeout(function () {
                    Site.Search.liveSearch($(e.currentTarget).parent().find(".livesearch"), $(e.currentTarget).val());
                }, 500);
            } else {
                $(e.currentTarget).parent().find(".livesearch").stop(true, true).fadeOut(400, function () {
                    $(this).html("");
                });
            }
        },
        liveSearch: function (target, query) {
            $.ajax({
                url: Site.Search.liveSearchUrl,
                global: false,
                type: "GET",
                dataType: "json",
                async: true,
                data: {
                    q: query
                },
                success: function (data) {
                    Site.Search.onSuccess(data, target);
                }
            });
        },
        onSuccess: function (d, target) {
            if (d.length > 0) {
                var l = d.length,
                    i = 0,
                    livesearch = "";
                for (i = 0; i < l; i++) {
                    livesearch += '<a data-history="true" href="' + d[i].slug + '">' + d[i].title + '</a>';
                }
                $(target).html(livesearch).stop(true, true).hide().fadeIn(400);
                Site.PushStates.overwriteLinks(target);
                $(target).find("a").on("click", Site.Search.hideLiveSearch);
            }
        },
        hideLiveSearch: function () {}
    };
    Site.Layout = {
        layouts: [],
        loaded: false,
        isMobile: false,
        init: function () {
            Site.Layout.checkMobile();
        },
        checkMobile: function () {
            if ($window.width() < 700 && Modernizr.touch) Site.Layout.isMobile = true;
            else {
                Site.Layout.isMobile = false;
            }
            return Site.Layout.isMobile;
        }
    };
    Site.Images = {
        onLoad: function () {
            $("img.loading").each(function (i, img) {
                $(img).imagesLoaded(function () {
                    $(img).removeClass("loading");
                });
            });
            if (!$("#feed").length) return;
            Site.Proto.bindTiles();
        }
    };
    Site.PushStates = {
        noChange: false,
        noLoad: false,
        noscroll: false,
        noRender: false,
        onlyModal: false,
        closeModal: false,
        tabClicked: false,
        title: "GSP",
        offset: 0,
        init: function () {
            if (!Modernizr.history) {
                Site.PushStates.addAnimationClasses();
                return;
            }
            History.Adapter.bind(window, 'statechange', Site.PushStates.onState);
        },
        cleanUp: function () {
            Site.PushStates.onlyModal = false;
            Site.PushStates.closeModal = false;
            $(".selected").removeClass("selected");
            Site.PushStates.addAnimationClasses();
            $body.removeClass("collections-open").removeClass("menu-open");
            Site.Menu.clearSearch();
        },
        addAnimationClasses: function () {
            var base = window.location.pathname.split("/")[1];
        },
        overwriteLinks: function (target) {
            if (!Modernizr.history || Site.Layout.isMobile) {
                return
            }
            var target = target || "body";
            $(target).find('a').not("[data-history='false']").not("[href^='#']").not("[href$='.jpg']").not("[href^='tel']").not("[target='_blank']").not("[href^='mailto:']").off().on("click.history", function (a) {
                a.preventDefault();
                Site.PushStates.postPage = false;
                Site.PushStates.gridPage = false;
                var self = $(this),
                    state = $(a.currentTarget).attr("href").replace("http://" + window.location.host, "");
                if (typeof (self.data("transition")) !== "undefined") {
                    if (self.data("transition") == "post") {
                        Site.PushStates.postPage = true;
                    } else if (self.data("transition") == "grid") {
                        Site.PushStates.gridPage = true;
                    }
                }
                History.pushState(null, null, state);
            });
            $(target).find('a[href^="http://"]').not('[href^="http://' + window.location.host + '"]').off(".history");
        },
        triggerAnimation: function () {},
        onState: function (e) {
            Site.PushStates.setTitle();
            if (Site.PushStates.noLoad) {
                Site.PushStates.noLoad = false;
                return;
            }
            var
            id = window.location.pathname.split("/")[Site.settings.path.pageIdPosition],
                loadTimeout = null,
                showLoader = function () {
                    $("body").addClass("loading");
                }, hideLoader = function () {
                    $("body").removeClass("loading");
                }, loadedData = "",
                animationTimer = null,
                onDataLoad = function (data) {
                    loadedData = data;
                    if (animationTimer == 0) handleLoadedData();
                }, handleLoadedData = function () {
                    Site.onUnload();
                    var $loadedData = $(loadedData),
                        $loadedMenu = $loadedData.find("#menu").find(".primary"),
                        $loadedContent = $loadedData.find("#content"),
                        title = $loadedContent.attr("data-title") || "",
                        code = "",
                        loadedClass = $loadedContent.attr("class") || "";
                    $("#menu").find(".primary").html($loadedMenu.html());
                    $("#grid-trigger").remove();
                    if ($loadedData.find("#grid-trigger").length) {
                        $("#menu-trigger").after($loadedData.find("#grid-trigger"));
                    }
                    if (Site.PushStates.postPage || Site.PushStates.gridPage) {
                        code = $loadedContent.html();
                        Site.PushStates.cleanUp();
                        Site.PushStates.setTitle(title);
                        $("#loadedcontent").html(code);
                        $("#oldcontent").remove();
                        $("#loadedcontent").children().eq(0).unwrap();
                        $body.removeClass("shift-content").removeClass("shift-content-right");
                        Site.onLoad();
                    } else {
                        $("#content").removeClass("show-content").html("");
                        code = $loadedContent.html();
                        Site.PushStates.cleanUp();
                        Site.PushStates.setTitle(title);
                        $("#content").removeClass("hide-content").addClass("show-content").append(code).data("title", title);
                        Site.onLoad();
                        Site.scrollToArticle(false);
                    }
                    if (location.hash != "" && location.hash != "#home") $("a[href='/']").attr("href", "/#home");
                    else $("a[href='/#home']").attr("href", "/");
                };
            if (Site.PushStates.noChange) {
                Site.PushStates.noChange = false;
                return;
            }
            if ($("[data-path='" + window.location.pathname + "']").length) {
                if (!Site.PushStates.noscroll) {
                    Site.scrollToArticle();
                    Site.PushStates.setTitle();
                }
            } else if (!Site.PushStates.noscroll) {
                animationTimer = 400;
                loadTimeout = setTimeout(function () {
                    animationTimer = 0;
                    if (loadedData != "") {
                        handleLoadedData();
                    }
                }, animationTimer);
                Site.PushStates.addAnimationClasses();
                if (!Site.PushStates.postPage && !Site.PushStates.gridPage) {
                    $("#content").removeClass("show-content").addClass("hide-content");
                } else {
                    $("#feed").find("video").each(function (i, v) {
                        var video = $(v).get(0);
                        video.pause();
                    });
                    $("#content").wrapInner('<div id="oldcontent">').append('<div id="loadedcontent"><div class="loader" /></div>').find(".loader").hide().delay(500).fadeIn(200);
                    setTimeout(function () {
                        $body.addClass("shift-content" + ((Site.PushStates.gridPage) ? "-right" : ""));
                    }, 50);
                }
                $.ajax({
                    url: window.location.pathname,
                    global: false,
                    type: "POST",
                    dataType: "html",
                    async: true,
                    data: {
                        ajax: 1
                    },
                    success: onDataLoad,
                    error: function () {
                        window.location = "http://" + window.location.host;
                    }
                });
                return;
            }
            if (!Site.PushStates.noscroll) {
                Site.scrollToArticle();
            } else {
                Site.PushStates.noscroll = false;
            }
            if (id == "") {
                $("html,body").stop(true, true).animate({
                    scrollTop: 0
                }, 600);
                $("#header").find("li").attr("class", "");
                if (document.title == "") {
                    document.title = Site.PushStates.title;
                }
                return;
            }
        },
        setTitle: function (title) {
            var _title = title || $("#content").data("title");
            document.title = _title;
        },
        goTo: function (loc, force) {
            var force = force || false;
            if (Modernizr.history) History.pushState(null, null, loc.replace("http://" + document.location.host, ""));
            else window.location = loc;
        }
    };
    Site.Post = {
        isNew: true,
        onUnload: function () {
            if ($("#post").data("infinitescroll")) {
                $("#post").infinitescroll('destroy');
                $("#post").data('infinitescroll', null);
            }
            Site.Post.portraitImages = null;
        },
        portraitImages: null,
        kill: function () {
            $("[data-slider]").each(function (i, slider) {
                if ($(slider).data("slider")) {
                    if (typeof ($(slider).data("slider").kill) == "function") {
                        $(slider).data("slider").kill();
                    }
                }
            });
        },
        onLoad: function (target) {
            if (!$("#post").length) {
                $("#grid-trigger").removeClass("show");
                $("#home-trigger").removeClass("show").removeClass("display");
                return;
            }
            $("#home-trigger").addClass("show").addClass("display");
            if (typeof (target) === "undefined") Site.Post.infinite();
            var _target = target || $("[data-slider]"),
                pW = $("#post").find(".post").eq(0).width();
            $("#grid-trigger").addClass("show");
            Site.Post.createSliders(_target);
            Site.Post.onResize();
        },
        createSliders: function (_target) {
            _target.each(function (i, slider) {
                if ($(slider).data("slider") && $(slider).find("ul").children().length > 1) {
                    var id = $(slider).attr("id"),
                        mode = $(slider).data("slider"),
                        current = (window.location.hash != "") ? window.location.hash.replace("#", "") * 1 - 1 : 0,
                        count = $(slider).find("ul").eq(0).children().length;
                    currentSwipe = new Swipe($(slider).get(0), {
                        startSlide: current,
                        speed: 400,
                        continuous: true,
                        disableScroll: false,
                        stopPropagation: false,
                        callback: Site.Post.updateSlideCount,
                        transitionEnd: Site.Post.updateSlideCount
                    });
                    if (mode != "new") {
                        $(slider).closest("article").find(".num").get(0).innerHTML = (current + 1) + " of " + count;
                    } else {
                        Site.Post.isNew = true;
                    }
                    var $controls = $(slider).closest("article").find(".controls"),
                        doNext = function (e) {
                            e.preventDefault();
                            Site.Post.beforeSlide(slider);
                            setTimeout(function () {
                                $(e.currentTarget).closest("article").find(".slide-wrap").data("slider").next();
                            }, 200);
                        }, doPrev = function (e) {
                            e.preventDefault();
                            Site.Post.beforeSlide(slider);
                            setTimeout(function () {
                                $(e.currentTarget).closest("article").find(".slide-wrap").data("slider").prev();
                            }, 200);
                        };
                    $controls.find(".next").on("click", doNext);
                    $controls.find(".prev").on("click", doPrev);
                    $(slider).data("slider", currentSwipe);
                }
            });
        },
        resizeTime: null,
        onResize: function (e, target) {
            Site.Post.doResize(e, target);
        },
        doResize: function (e, target) {
            var pW = $("#post").find(".slide-media").eq(0).width(),
                aspect = ($window.width() > 320) ? 1.78 : 1.33,
                playerHeight = Math.round(pW / aspect);
            $("#post").find("iframe, .vimeo-player, .portrait, .slide-media").height(playerHeight);
        },
        updateSlideCount: function (current, li, slider) {
            if (!$("#post").length) return;
            if (Site.Post.isNew) {
                Site.Post.afterSlide(current, li, slider);
                return;
            }
            var slider = slider || $(li).closest("[data-slider]"),
                swipe = slider.data("slider"),
                max = swipe.getNumSlides(),
                alt = "";
            if (current >= max) {
                current -= max;
            }
            if ($(li).find("img").length) alt = $(li).find("img").data("title") || $(li).find("img").attr("alt");
            else if ($(li).find("iframe").length) alt = $(li).find("iframe").data("title");
            slider.parent().find(".num").get(0).innerHTML = (current + 1) + " of " + max;
            slider.parent().find("h3").get(0).innerHTML = alt;
        },
        beforeSlide: function (swipe) {
            $(swipe).find("li").find(".slide-content").animate({
                opacity: 0
            }, 200);
        },
        afterSlide: function (current, li, slider) {
            var article = $(li).closest("article");
            setTimeout(function () {
                $(li).find(".slide-content").animate({
                    opacity: 1
                }, 200);
            }, 400);
            Site.Vimeo.destroyPlayers($(li).closest("ul"));
            if ($(li).find(".vimeo-player").length) {
                var player = $(li).find(".vimeo-player");
                Site.Vimeo.showPlayer(player);
            }
            if (!Modernizr.history) return;
            if ($(li).data("slide-path")) {
                var path = $(li).data("slide-path"),
                    title = $(li).data("slide-title");
                Site.PushStates.noLoad = true;
                Site.PushStates.goTo(path, true);
                Site.PushStates.setTitle(title);
                Site.PushStates.noLoad = false;
                article.data("path", path);
                article.data("title", title);
            }
        },
        infinite: function () {
            $('#post').infinitescroll({
                loading: {
                    img: "/public/themes/gsp/images/ajax-loader.gif",
                    finishedMsg: "",
                    msgText: ""
                },
                nextSelector: "#pages a",
                navSelector: "#pages",
                extraScrollPx: 100,
                itemSelector: ".post",
                bufferPx: 500
            }, function (appended) {
                if ($(appended).find("[data-slider]").length)
                    Site.Post.onLoad($(appended).find("[data-slider]"));
                Site.Vimeo.createPlayers(appended);
            });
        }
    };
    Site.Tracking = {
        onLoad: function () {
            $("[data-track-category]").off().on("click", Site.Tracking.onClick);
        },
        onClick: function (e) {
            var category = $(e.currentTarget).data("track-category") || "",
                label = $(e.currentTarget).data("track-label") || "",
                action = $(e.currentTarget).data("track-action") || "";
            if (typeof (_trackEvent) === "undefined") return;
            _trackEvent(category, action, label);
        }
    };
    Site.Vimeo = {
        onLoad: function () {
            Site.Vimeo.createPlayers();
        },
        createPlayers: function (target) {
            var $target = target || "#post";
            $target = $($target);
            $target.find(".vimeo-player").each(function (e, player) {
                var $player = $(player),
                    imageSrc = ($player.find("img").length) ? $player.find("img").attr("src") : $player.data("vimeo-image");
                $player.data("vimeo-image", imageSrc).append('<span class="icon" />');
                if ($player.closest("li").index() != 0) {
                    $player.find("img").remove();
                }
            });
            $target.find(".slideshow").find("li:first-child").find(".vimeo-player").each(function (n, p) {
                Site.Vimeo.showPlayer(p);
            });
        },
        showPlayer: function (_player) {
            var player = $(_player),
                id = player.data("vimeo-id"),
                code = '<iframe id="' + id + '" src="http://player.vimeo.com/video/' + id + '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;api=1&amp;player_id=' + id + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            if (!player.find("img").length)
                player.append('<img class="vimeo-image" src="' + player.data("vimeo-image") + '">');
            player.find("img").imagesLoaded(function () {
                player.addClass("loaded").removeClass("is-loading");
            });
            player.addClass("is-loading").find("img, .icon").off().on("click", function () {
                if (player.hasClass("paused")) {
                    var iframe = $('#' + id)[0],
                        vim_player = $f(iframe);
                    vim_player.api("play");
                    player.removeClass("paused");
                    return;
                }
                player.addClass("playing");
                player.append(code);
                var iframe = $('#' + id)[0],
                    vim_player = $f(iframe);
                vim_player.addEvent('ready', function () {
                    if (!Modernizr.touch || Site.Layout.isMobile) {
                        vim_player.api("play");
                    }
                    if (Modernizr.touch) {
                        vim_player.addEvent('pause', function () {
                            player.addClass("paused");
                        });
                    }
                    vim_player.addEvent('finish', function () {
                        player.addClass("paused");
                    });
                });
            });
        },
        destroyPlayers: function (_target) {
            var target = _target || "body";
            $(target).find("[data-vimeo-id]").removeClass("playing").each(function (i, self) {
                $(self).find("iframe").remove();
            });
        }
    };
    Site.Triggers = {
        que: true,
        init: function () {
            Site.Triggers.onResize();
        },
        onLoad: function () {
            Site.Triggers.onResize();
        },
        onResize: function () {
            $("#menu-trigger").css({
                top: Site.Grid.columnWidth + 20 + "px"
            }).addClass("display");
            $("#grid-trigger").css({
                top: Site.Grid.columnWidth + 180 + "px"
            }).addClass("display");
            $("#home-trigger").css({
                top: Site.Grid.columnWidth + 100 + "px"
            });
        }
    };
    Site.Grid = {
        onUnload: function () {
            if ($("#feed").data("infinitescroll")) {
                $("#feed").infinitescroll('destroy');
                $("#feed").data('infinitescroll', null);
            }
        },
        onLoad: function () {
            if (!$("#feed").length) return;
            Site.Grid.oldWidth = null;
            Site.Grid.setSizes();
            Site.Grid.onResize(1);
            $("#feed").find("img.loading").each(function (i, img) {
                $(img).imagesLoaded(function () {
                    $(img).removeClass("loading");
                });
            });
            $('#feed').infinitescroll({
                loading: {
                    img: "/public/themes/gsp/images/ajax-loader.gif",
                    finishedMsg: "",
                    msgText: ""
                },
                nextSelector: "#pages a",
                navSelector: "#pages",
                extraScrollPx: 600,
                itemSelector: ".grid-cell",
                prefill: true,
                bufferPx: 400
            }, function (appended) {
                $(appended).addClass("just-added").addClass("ajax-loaded");
                Site.Grid.setSizes();
                $("#feed").packery('appended', appended);
                Site.PushStates.overwriteLinks($(appended));
                setTimeout(function () {
                    $(appended).removeClass("just-added");
                    Site.Images.onLoad();
                    Site.Grid.handleVideo();
                }, 750);
            });
            Site.Grid.handleVideo();
        },
        doMasonry: function () {
            var $container = $("#feed"),
                $logo = (Site.browser == "oldIE") ? $("#logo") : $container.find("#logo, .contact"),
                packeryConfig = {
                    itemSelector: '.grid-cell',
                    columnWidth: Site.Grid.columnWidth,
                    gutter: Site.Grid.gutterWidth,
                    rowHeight: Site.Grid.columnWidth
                };
            packeryConfig.stamp = $logo;
            $container.packery(packeryConfig).addClass("packed");
        },
        gutterWidth: 20,
        columnWidth: 220,
        time: null,
        onResize: function (timeout) {
            if (!$("#feed").length) return;
            clearTimeout(Site.Grid.time);
            var _timeout = timeout || 500;
            Site.Grid.time = setTimeout(Site.Grid.doResize, _timeout);
        },
        oldWidth: null,
        doResize: function () {
            if (Site.Grid.oldWidth == $window.width()) return;
            var $container = $("#feed");
            Site.Grid.oldWidth = $window.width();
            if ($("#feed").data("packery")) $container.removeClass("packed").packery("destroy");
            Site.Grid.setSizes();
            if (!$container.data("packery")) {
                Site.Grid.doMasonry();
            }
            Site.Triggers.onResize();
        },
        setSizes: function () {
            if (!$("#feed").length) return;
            var $feed = $("#feed"),
                gutter = $feed.find(".grid-cell:eq(0)").css("marginLeft").replace("px", "") * 2,
                ww = $window.width() - gutter,
                cols = (gutter == 16) ? 3 : Math.max(2, Math.round(ww / (220 + gutter))),
                colWidth = Math.round(ww / cols) - gutter;
            Site.Grid.gutterWidth = gutter;
            Site.Grid.columnWidth = colWidth;
            $feed.find(".col-2").css({
                width: (2 * colWidth + gutter) + "px"
            });
            $feed.find(".col-1").css({
                width: colWidth + "px"
            });
            $feed.find(".row-2").css({
                height: (2 * colWidth + gutter) + "px"
            });
            $feed.find(".row-1").css({
                height: colWidth + "px"
            });
            $("#logo").css({
                width: colWidth + "px",
                height: colWidth + "px",
                top: colWidth + gutter * 2 + "px"
            });
            $(".contact").css({
                width: colWidth + "px",
                height: colWidth + "px",
                top: colWidth * 2 + gutter * 3 + "px"
            }).eq(1).css({
                left: colWidth + gutter + "px"
            });
        },
        handleVideo: function () {
            if (!Modernizr.video) return;
            $("#feed").find("video").each(function (i, v) {
                var video = $(v).get(0);
                if ($(v).data("proto")) {
                    $(v).closest("div").hover(function () {
                        v.play();
                    }, function () {
                        v.pause();
                    });
                } else
                    video.play();
                video.addEventListener("ended", video.play);
            });
        }
    };
    Site.Map = {
        ST: "",
        distance_var: "Distance:",
        ROOT: "http://" + window.location.host + "/",
        activeWindow: null,
        map: null,
        maps: [],
        startCords: Array(52.404062, 16.925967),
        defaultZoom: 18,
        points: [],
        mapContainer: "map",
        $map: "",
        onLoad: function () {
            if (!$("[data-map]").length) return;
            $("a[data-coords]").off().on("click", Site.Map.createMapInstance);
            $("[data-map]").each(function (i, map) {
                Site.Map.maps.push({
                    container: $(map),
                    points: [],
                    marker: null
                });
            });
            $.each(Site.Map.maps, function (i, map) {
                if (map.container.data("points")) map.points = map.container.data("points");
                if (map.container.data("marker")) map.marker = map.container.data("marker");
            });
            if (typeof (google) === 'undefined') {
                var cache = $.ajaxSettings.cache;
                $.ajaxSettings.cache = true;
                $.getScript('http://maps.googleapis.com/maps/api/js?sensor=true&callback=GSP.onMapScriptLoad');
                $.ajaxSettings.cache = cache;
            } else {
                Site.Map.onScriptLoad();
            }
        },
        onScriptLoad: function () {
            $.each(Site.Map.maps, function (i, map) {
                var $map = map.container;
                if (typeof ($map.data("coords")) !== "undefined") {
                    map.points = [{
                        "m_id": 1,
                        "m_address": $map.data("info") || "",
                        "m_name": $map.data("name") || "",
                        "m_lat": $map.data("coords").split(",")[0],
                        "m_lng": $map.data("coords").split(",")[1]
                    }];
                }
            });
            Site.Map.createMap();
        },
        createMap: function () {
            Site.Map.mapOptions["mapTypeControlOptions"] = {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'mapstyle']
            };
            Site.Map.mapOptions["mapTypeId"] = google.maps.MapTypeId.ROADMAP;
            $.each(Site.Map.maps, Site.Map.drawMap);
            Site.Map.setCenter();
            $window.bind("resize", Site.Map.setCenter);
        },
        mapOptions: {
            zoom: 5,
            mapTypeControlOptions: null,
            center: true,
            navigationControl: true,
            scaleControl: false,
            mapTypeControl: false,
            scrollwheel: false,
            mapTypeId: null,
            scaleControl: (Modernizr.touch) ? false : true,
            streetViewControl: (Modernizr.touch) ? false : true,
            zoomControl: (Modernizr.touch) ? false : true
        },
        customStyle: {
            featureType: 'all',
            stylers: [{}, {}, {
                "weight": 0.1
            }, {}]
        },
        applyMapStyle: function (map) {
            var customType = new google.maps.StyledMapType([Site.Map.customStyle], {
                name: "mapstyle"
            });
            map.map.mapTypes.set('mapstyle', customType);
            map.map.setMapTypeId('mapstyle');
        },
        initClusterMap: function () {},
        markers: Array(),
        drawMap: function (i, map) {
            map.mapOptions = Site.Map.mapOptions;
            map.markers = {};
            if (map.points.length > 0) {
                var gPoints = [];
                for (var i = 0; i < map.points.length; i++) {
                    gPoints[i] = new google.maps.LatLng(map.points[i]["m_lat"], map.points[i]["m_lng"]);
                }
                map.mapOptions["center"] = gPoints[0];
                map.map = new google.maps.Map(map.container.get(0), map.mapOptions);

                function createMarker(p, t, c, i, id, lat, lng) {
                    var settings = {
                        position: p,
                        title: t,
                        c: c,
                        map: map.map
                    };
                    if (map.marker) {
                        settings.icon = {
                            url: map.marker,
                            scaledSize: new google.maps.Size(190, 70),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(190 / 2, 70)
                        };
                        settings.optimized = false;
                    }
                    var marker = new google.maps.Marker(settings);
                    map.markers[id] = marker;
                    google.maps.event.addListener(marker, 'click', function () {
                        window.open("https://www.google.com/maps/preview#!q=" + lat + "%2C" + lng);
                    });
                }
                for (var i = 0; i < gPoints.length; i++) {
                    createMarker(gPoints[i], map.points[i]["m_name"], map.points[i]["m_address"], i, map.points[i]["m_id"], map.points[i]["m_lat"], map.points[i]["m_lng"]);
                }
                Site.Map.applyMapStyle(map);
            }
        },
        overrideTouch: function () {
            var dragFlag = false,
                start = 0,
                end = 0,
                touchStart = function (e) {
                    e = e.originalEvent;
                    dragFlag = true;
                    start = e.touches[0].pageY;
                }, touchEnd = function () {
                    dragFlag = false;
                }, touchMove = function (e) {
                    e = e.originalEvent;
                    if (!dragFlag) {
                        return;
                    }
                    end = e.touches[0].pageY;
                    window.scrollBy(0, (start - end));
                };
            $("#" + Site.Map.mapContainer).bind({
                "touchstart": touchStart,
                "touchend": touchEnd,
                "touchmove": touchMove
            });
        },
        setCenter: function (e) {
            var $obj, centerStr;
            $.each(Site.Map.maps, function (i, map) {
                if (map.container.data("coords")) {
                    centerStr = ["", map.container.data("coords").split(",")[0], map.container.data("coords").split(",")[1]];
                } else {
                    centerStr = ["", Site.Map.startCords[0], Site.Map.startCords[1]];
                }
                map.map.setZoom(Site.Map.defaultZoom);
            });
        }
    };
    Site.Proto = {
        bindTiles: function () {
            if (Modernizr.touch || !Modernizr.csstransforms3d) return;
            $("#feed").find("div").not(".has-video").off("mousemove").on("mousemove", Site.Proto.pan).on("mouseleave", Site.Proto.removePan);
        },
        panTime: null,
        pan: function (e) {
            var self = $(e.currentTarget).find("img"),
                div = $(e.currentTarget),
                x = e.offsetX || e.pageX - div.offset().left,
                y = e.offsetY || e.pageY - div.offset().top,
                w = self.width(),
                h = self.height(),
                d = 0.05 * Math.min(w, h),
                px = (x / w - 0.5) * 2,
                py = (y / h - 0.5) * 2,
                tx = Math.round(-px * d),
                ty = Math.round(-py * d),
                styleTiles = function () {
                    self.attr("style", "-webkit-transform: translate3d(" + tx + "px, " + ty + "px, 0px) scale(1.1); transform: translate3d(" + tx + "px, " + ty + "px, 0) scale(1.1); -moz-transform: translate3d(" + tx + "px, " + ty + "px, 0px) scale(1.1)");
                };
            clearTimeout(Site.Proto.panTime);
            Site.Proto.panTime = setTimeout(styleTiles, 25);
        },
        removePan: function (e) {
            var self = $(e.currentTarget).find("img"),
                div = $(e.currentTarget),
                tx = 0,
                ty = 0;
            if (div.hasClass("has-video")) return;
            self.attr("style", "-webkit-transform: translate3d(" + tx + "px, " + ty + "px, 0) scale(1); transform: translate3d(" + tx + "px, " + ty + "px, 0) scale(1); -moz-transform: translate3d(" + tx + "px, " + ty + "px, 0px) scale(1)");
        }
    };
    Site.Scrolling = {
        init: function () {
            if (!Modernizr.history) return;
            $window.on("scroll", Site.Scrolling.onScroll);
        },
        scrollTime: null,
        onScroll: function () {
            clearTimeout(Site.Scrolling.scrollTime);
            Site.Scrolling.scrollTime = setTimeout(Site.Scrolling.checkPath, 250);
        },
        checkPath: function () {
            var subpages = $("[data-path]"),
                l = subpages.length,
                i = l - 1,
                found = 0;
            if (l == 0) return;
            while (i >= 0 && found == 0) {
                if ($window.scrollTop() >= $(subpages[i]).offset().top - 100) {
                    found = i;
                }
                i--;
            }
            if (found >= 0) {
                var subpage = $(subpages[found]);
                Site.PushStates.noLoad = true;
                Site.PushStates.goTo(subpage.data("path"), true);
                Site.PushStates.setTitle(subpage.data("title"));
                Site.PushStates.noLoad = false;
            }
        }
    };
    Site.Mobile = {
        resizeTime: null,
        orientation: false,
        init: function () {
            var doOnOrientationChange = function () {
                Site.Post.kill();
                $("#content").html('<div class="loader"></div>');
                Site.Mobile.orientation = true;
                clearTimeout(Site.Mobile.resizeTime);
                Site.Mobile.resizeTime = setTimeout(function () {
                    Site.Mobile.orientation = false;
                    Site.PushStates.onState();
                }, 300);
            };
            window.addEventListener('orientationchange', doOnOrientationChange);
        }
    };
    $window = $(window), $body = $("body"), Site.init();
    return {
        mobile: Site.Layout.checkMobile,
        onMapScriptLoad: Site.Map.onScriptLoad
    }
}(jQuery);