/***************************
Project Name: Volo
Description: Ads Posting
Develop By: JP Mendoza
Company: Mayon Studios Inc.
License: Private Use v1
***************************/

function Volo() {
	
	var _url = $('base').attr('href');
	var browser_WT = $(window).width();
	
	this.search = function() {
		keywords = $('#search-input').val();
		window.location = _url + 'search/results?keywords='+encodeURIComponent(keywords)+'&window='+browser_WT;
		
		//keyword = $('#search-input').val();
		//datas = {keywords : keyword, page : 1, type : 'my_searches'};
		//url = 'search';
		//this.requestApi(url, datas, 1);
	}
	
	this.autocomplete = function() {
		keyword = $('#search-input').val();
		datas = {keywords : keyword};
		url = 'search-autocomplete';
		this.requestApi(url, datas, 2);
	}
	
	this.explore = function() {
		datas = {keywords : 'a'};
		url = 'search-explore';
		this.requestApi(url, datas, 3);
	}
	
	this.logout = function() {
		window.location = _url+'auth/logout';	
	}
	
	this.login = function() {
		em = $('#auth-email').val();
		ps = $('#auth-password').val();
		datas = {email : em, password : ps};
		url = 'login';
		this.requestAuth(url, datas, 1);
	}
	
	this.login_window = function() {
		login_window(350, 330);	
	}
	
	this.register_window = function() {
		register_window(350, 330);	
	}
	
	this.register = function() {
		r = '#registration-';
		em = $(r+'email').val();
		ps = $(r+'password').val();
		fn = $(r+'firstname').val();
		ln = $(r+'lastname').val();
		bd = $(r+'dob').val();
		gn = $('input[name=registration_gender]:checked').val();
		
		datas = {email : em, password : ps, firstname : fn, lastname : ln, dob : bd, gender : gn}
		url = 'register';
		this.requestAuth(url, datas, 2);
	}
	
	this.post_window = function() {
		post_window(500, 475);	
	}
	
	this.create_post = function() {
		pc = $('#post-category').val();
		pt = $('#post-title').val();
		pd = $('#post-description').val();
		pl = $('#post-location').val();
		pp = $('#post-price').val();
		pa = ($('#post-dollar').is(':checked')) ? 1 : 0;
		ps = ($('#post-status').is(':checked')) ? 1 : 0;
		datas = {category : pc, title : pt, description : pd, location : pl, price : pp, status : ps, accept : pa};
		url = 'post';
		this.requestApi(url, datas, 5);
	}
	
	this.requestAuth = function(u, datas, result) {
		$.ajax({
				type : 'POST',
				cache:false,
				data : datas,
				dataType : "json",
				crossDomain:true,
				beforeSend : function(){},
				url : _url+'auth/'+u,
				success: function(data) {
					datas = data; //JSON.parse(data);
					switch(result) {
						case 1: //login
							if(datas.success === true)
								window.location = _url;
							else {
								alert(datas.error);	
							}
						break;
						case 2: //register
							if(datas.success === true) {
								window.location = _url;
							}
							else {
								err = datas.success;
								errm = 'Error: ';
								if('message' in err) {
									errm += err['message']+' ';
								}
								
								if('email' in err) {
									errm += err['email']+' ';
								}
								
								if('firstname' in err) {
									errm += err['firstname']+' ';
								}
								
								if('lastname' in err) {
									errm += err['lastname']+' ';
								}
								
								if('password' in err) {
									errm += err['password']+' ';
								}
								
								alert(errm)
							}
						break;
					}
					
				},
				complete: function() {
					
				}
		});
	}
	
	this.requestApi = function(u, datas, result) {
		var $container = $('#post-items');
		$.ajax({
				type : 'POST',
				cache:false,
				data : datas,
				dataType : "json",
				crossDomain:true,
				beforeSend : function(){},
				url : _url+'request/api/'+u,
				success: function(data) {
					datas = data; //JSON.parse(data);
					switch(result) {
						case 1:
							
							$container.masonry( 'destroy' );
							search_result(datas);
							$('#auto-complete').hide();		
						break;
						case 2:
							autocomplete_result(datas)
							if('keywords' in datas) {
								$('#auto-complete').show();	
							}
							else {
								$('#auto-complete').hide();	
							}
						break;
						case 3:
							//$container.masonry( 'destroy' );
							explore_result(datas);
						break;
						case 4:
							if(datas.success === true)
							alert('')
						break;
						case 5:
							if(datas.success === true)
								
								u = 'http://54.193.15.236/volo_photo/upload/testupload/';
								$('#upload-image').attr('action', u+datas.post_id);
								$('#upload-image #post-id').val(datas.post_id);
								setTimeout(function() {
									$( "#upload-image" ).submit();
								}, 2000);
								alert('post saved '+ datas.post_id)
						break;
					}
					
				},
				complete: function() {
					
				}
		});
	}

	function search_result(data) {
		
		//variable
		var browser_WT = $(window).width();
		var random_col = Array(4,5);
		var img_orientation = Array('S','L','P');
		var post_margin = 10;
		var post_container = parseInt(browser_WT * .9);
		var imgWT=0, imgHT=0, maxWT=0, maxHT=0, WT=0, HT=0, FWT=0, FHT=0, column=0, ratio=0;
		var results = data.results.length - 1;
		var post_items = $('#post-items');
		var content = '';
		var post_img_url = '';
		var profile_img_url = '';
		var img_url = '';
		
		//clear post items container
		post_items.html('');
		
		column = 4;
		maxWT = parseInt((post_container - (column * 10)) / column);
		maxHT = maxWT;
		counter = results;
		for ( i in data.results) {
			if ( "feed" in data.results[i] ) {
				post_item = data.results[i]['feed'];
				
				
				post_img_url = 'http://54.193.15.236/volo_photo/resize/index/post/';
				profile_img_url = 'http://54.193.15.236/volo_photo/resize/index/profile/60x60/'+post_item['user_profile_image'];
				
				imgWT = post_item['width'];
				imgHT = post_item['height'];
				
				/*if(column ==0) {
					column = shuffle(random_col)[0];
					maxWT = parseInt((post_container - (column * 10)) / column);
					maxHT = maxWT;
				}*/
				
				orientation = shuffle(img_orientation)[0];
				
				if(counter > 0) {
					
					//get image orienteation
					if(imgWT > imgHT) {
						if(imgWT > maxWT) {
							ratio = maxWT / imgWT;
							HT=parseInt(imgHT*ratio);
						}
						else if(maxWT > imgWT) {
							ratio = imgWT / maxWT;
							HT=parseInt(maxHT*ratio);
						}
						WT=maxWT;
					}
					else if(imgHT > imgWT) {
						if(imgHT > maxHT) {
							ratio = maxHT / imgHT;
							WT=parseInt(imgWT*ratio);
						}
						else if(maxHT > imgHT) {
							ratio = imgHT / maxHT;
							WT=parseInt(maxWT*ratio);
						}
						HT=maxWT;
					}
					
					if(orientation == 'S') {
						FWT = WT;
						FHT = HT;
					}
					else if(orientation == 'L') {
						FWT = WT;
						FHT = HT;
					}
					else if(orientation == 'P') {
						FWT = WT;
						FHT = HT;
					}
					
					img_url = post_img_url+FWT+'x'+FHT+'/'+post_item['image_url'];
				}
				
				
				
				
				if(post_item['image_url'] != '') {
					content += '<div class="post-item" style="width:'+FWT+'px; '+FHT+'px;"><div class="post-content">'+
							   '<div class="post-image"><img src="'+img_url+'"></div>'+
							   //'<div class="post-review"><span>31</span></div>'+
							   '<div class="post-user"><div><img src="'+profile_img_url+'"></div></div>'+
							   '<div class="post-info"><div class="post-info-content"><div class="title"><span>'+post_item['title']+'</span></div>'+
							   '<div class="price"><span>'+post_item['price']+'</span></div></div></div>'+
							   '</div></div>';
				}
				counter--;
			}	
		}
		
		
		$('#post-items').append(content); msnry(10);
	}
	
	function explore_result(data) {
		
		//variable
		var browser_WT = $(window).width();
		var random_col = Array(4,5);
		var img_orientation = Array('S','L','P');
		var post_margin = 10;
		var post_container = parseInt(browser_WT * .9);
		var imgWT=0, imgHT=0, maxWT=0, maxHT=0, WT=0, HT=0, FWT=0, FHT=0, column=0, ratio=0;
		var results = data.results.length - 1;
		var post_items = $('#post-items');
		var content = '';
		var post_img_url = '';
		var profile_img_url = '';
		var img_url = '';
		var totalWT=0;
		var currentWT=0;
		
		//clear post items container
		post_items.html('');
		
		column = 4; //shuffle(random_col)[0];
		maxWT = parseInt((post_container - (column * 10)) / column);
		totalWT = maxWT * 4;
		maxHT = maxWT;
		counter = results;
		for ( i in data.results) {
			if ( "feed" in data.results[i] ) {
				post_item = data.results[i]['feed'];
				
				
				post_img_url = 'http://54.193.15.236/volo_photo/resize/index/post/';
				profile_img_url = 'http://54.193.15.236/volo_photo/resize/index/profile/60x60/'+post_item['user_profile_image'];
				
				
				if(post_item['image_url'] != '') {
					imgWT = post_item['width'];
					imgHT = post_item['height'];
				}
				else {
					imgWT = maxWT;
					imgHT = maxHT;
				}
				
				orientation = shuffle(img_orientation)[0];
				
				if(counter > 0) {
					
					//get image orienteation
					if(imgWT > imgHT) {
						if(imgWT > maxWT) {
							ratio = maxWT / imgWT;
							HT=parseInt(imgHT*ratio);
						}
						else if(maxWT > imgWT) {
							ratio = imgWT / maxWT;
							HT=parseInt(maxHT*ratio);
						}
						WT=maxWT;
					}
					else if(imgHT > imgWT) {
						if(imgHT > maxHT) {
							ratio = maxHT / imgHT;
							WT=parseInt(imgWT*ratio);
						}
						else if(maxHT > imgHT) {
							ratio = imgHT / maxHT;
							WT=parseInt(maxWT*ratio);
						}
						HT=maxWT;
					}
					else if(imgHT == imgWT) {
						WT=maxWT
						HT=maxHT;
					}
					
					if(orientation == 'S') {
						FWT = WT; //parseInt(WT / 2);
						FHT = HT; //parseInt(HT / 2);
					}
					else if(orientation == 'L') {
						FWT = WT * 2;
						FHT = HT;
					}
					else if(orientation == 'P') {
						FWT = WT;
						FHT = HT * 2;
					}
					currentWT += FWT;
					img_url = (post_item['image_url'] != '') ? post_img_url+FWT+'x'+FHT+'/'+post_item['image_url'] : no_image(FWT, FHT);
				}
				
				
				
				if(currentWT != totalWT) {
					
					content += '<div class="post-item" style="width:'+FWT+'px; '+FHT+'px;"><div class="post-content">'+
							   '<div class="post-image"><img src="'+img_url+'"></div>'+
							   //'<div class="post-review"><span>31</span></div>'+
							   '<div class="post-user"><div><img src="'+profile_img_url+'"></div></div>'+
							   '<div class="post-info"><div class="post-info-content"><div class="title"><span>'+post_item['title']+'</span></div>'+
							   '<div class="price"><span>'+post_item['price']+'</span></div></div></div>'+
							   '</div></div>';

				}
				else {
					currentWT = 0;
				}
				counter--;
			}	
		}
		
		
		$('#post-items').append(content); msnry(10);
	}
	
	function autocomplete_result(data) {
		$('#ac-container').html('');
		var content = '';
		for ( i in data.keywords) {
			content += '<li>'+data.keywords[i]+'</li>';
		}
		$('#ac-container').append(content)
	}
	
	function no_image(w,h) {
		return 'https://54.193.15.236/volo_photo/resize/index/post/'+w+'x'+h+'/large.jpg'	
	}
	
	function shuffle(o) {
		for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		return o;
	}
	
	function login_window(wc, ac) {
		cont = '<div class="window-curtain"><div class="window-content" style="width:'+wc+'px;"><div class="close-window-container right"><a class="close-window">X</a></div><div class="clear"></div><div class="append-content" style="width:'+ac+'px;">'+
				  '</div></div></div>';
		$('body').append(cont);
		$('.append-content').load(_url+'auth/logform');
	}
	
	function post_window(wc, ac) {
		cont = '<div class="window-curtain"><div class="window-content" style="width:'+wc+'px;"><div class="close-window-container right"><a class="close-window">X</a></div><div class="clear"></div><div class="append-content" style="width:'+ac+'px;">'+
				  '</div></div></div>';
		$('body').append(cont);
		$('.append-content').load(_url+'post/create');
	}
	
	function register_window(wc, ac) {
		cont = '<div class="window-curtain"><div class="window-content" style="width:'+wc+'px;"><div class="close-window-container right"><a class="close-window">X</a></div><div class="clear"></div><div class="append-content" style="width:'+ac+'px;">'+
				  '</div></div></div>';
		$('body').append(cont);
		$('.append-content').load(_url+'auth/regsform');
	}
	
	//basic upload function using html5 FormData
	function upload() {
		var form = $('#upload-image');
		var formdata = false;
		if (window.FormData){
			formdata = new FormData(form[0]);
		}

		$.ajax({
			url 		 : 'http://localhost:4100/volo/request/upload',
			data        : formdata ? formdata : form.serialize(),
			cache       : false,
			contentType : false,
			processData : false,
			type        : 'POST',
			success     : function(data, textStatus, jqXHR){
				data = JSON.parse(data);
				//if(data.success === true)
					alert(textStatus);
			}
		});
	}
	
}