
// Masonry corner stamp modifications
$.Mason.prototype.resize = function() {
	this._getColumns();
	this._reLayout();
};
$.Mason.prototype._reLayout = function( callback ) {
	var freeCols = this.cols;
	if ( this.options.cornerStampSelector ) {
		var $cornerStamp = this.element.find( this.options.cornerStampSelector ),
		cornerStampX = $cornerStamp.offset().left -
		( this.element.offset().left + this.offset.x + parseInt($cornerStamp.css('marginLeft')) );
		freeCols = Math.floor( cornerStampX / this.columnWidth );
	}
	// reset columns
	var i = this.cols;
	this.colYs = [];
	while (i--) {
		this.colYs.push( this.offset.y );
	}
	for ( i = freeCols; i < this.cols; i++ ) {
		this.colYs[i] = this.offset.y + $cornerStamp.outerHeight(true);
	}
	// apply layout logic to all bricks
	this.layout( this.$bricks, callback );
}; 
  
$(function(){

	var _acTimer = 0;
	
	volo = new Volo;
	//volo.explore();
	msnry(10);
	
	$('#auto-complete').hide();
	
	jQuery("#search-input").keypress(function (e) {
        if (e.keyCode == 13) {
            volo.search();
			$('#auto-complete').hide();
			clearTimeout(_acTimer);
			$('input').blur();
        } 
		else {
			if($("#search-input").val() != '') {
				clearTimeout(_acTimer);
				_acTimer = setTimeout(function() {
					volo.autocomplete();
				},300);
			}
		}
    });
	
	$('.close-window').live('click', function() {
		$('.window-curtain').hide();
		$('.window-curtain').remove();
	});
	
	//call login window
	$('#login-button').on('click', function() {
		volo.login_window();
	});
	
	$('#register-button').on('click', function() {
		volo.register_window();
	});
	
	$('#auth-login-submit').live('click', function() {
		volo.login();
	});
	
	$('#registration-submit').live('click', function() {
		if($('#registration-accept').is(":checked")) {
			volo.register();
			//alert('success');
		}
		else {
			alert('Please accept terms and conditions');
		}
	});
	
	$('#logout-button').on('click', function() {
		volo.logout();
	});
	
	//call create post
	$('#create-post').on('click', function() {
		volo.post_window();
	});
	
	$('#post-save').live('click', function() {
		volo.create_post();
	});

//

	search_x = $('#auto-complete').offset().left;
    search_y = $('#auto-complete').offset().top;
    search_height = $('#auto-complete').height();
    $('#ac-container').css({
        'left': search_x,
        'top': (search_y + search_height + 5)
    });
	
    //arrow key navigation
    start = -1;
    $(document).on('keydown', document,function(e){
        if(e.keyCode == 38){
			if($('#search-input').val() != '') {
				if (start == -1){
					start = ($('#ac-container li').size() - 1);  
				}else{
					start--;
					if (start < 0){
						start = ($('#ac-container li').size() - 1);  
					}
				}
				ac = $('#ac-container li').eq(start).html();
				$('#ac-container li').removeClass('red');
				$('#ac-container li').eq(start).addClass('red');
				$("#search-input").val(ac);
			}
			else {
				$('#auto-complete').hide();	
			}
        }

        if(e.keyCode == 40){
			if($('#search-input').val() != '') {
				if (start == -1){
					start = 0;  
				}else{
					start++;
					if (start > ($('#ac-container li').size() - 1)){
						start = 0;  
					}
				}
				ac = $('#ac-container li').eq(start).html();
				$('#ac-container li').removeClass('red');
				$('#ac-container li').eq(start).addClass('red');
				$("#search-input").val(ac);
			}
			else {
				$('#auto-complete').hide();
			}
        }
	});

}); //end of init

function msnry(margin) {
	var $container = $('#post-items');
	
	$container.imagesLoaded(function(){
		$container.masonry({
			itemSelector: '.post-item',
			columnWidth: 10,/*function( containerWidth ) {
        		return containerWidth / 5
      		},*/
			gutterWidth: 10,
			isAnimated: true,
			animationOptions: {
				duration: 600,
				easing: 'linear',
				queue: false
				//cornerStampSelector: '.aa'
			}
		});
	});
	return false;
}