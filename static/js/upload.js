	//basic upload function using html5 FormData
	function upload() {
		var form = $('#form');
		var formdata = false;
		if (window.FormData){
			formdata = new FormData(form[0]);
		}
		
		var formAction = form.attr('action');
		$.ajax({
			url         : 'upload.php',
			data        : formdata ? formdata : form.serialize(),
			cache       : false,
			contentType : false,
			processData : false,
			type        : 'POST',
			success     : function(data, textStatus, jqXHR){
				// Callback code
			}
		});
	}